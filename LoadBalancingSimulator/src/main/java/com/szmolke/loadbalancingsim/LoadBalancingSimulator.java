package com.szmolke.loadbalancingsim;

import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.impl.*;
import com.szmolke.loadbalancingsim.distribution.CloudletsCreator;
import com.szmolke.loadbalancingsim.distribution.CloudletsDistributor;
import com.szmolke.loadbalancingsim.experiment.ExperimentsEvaluator;
import com.szmolke.loadbalancingsim.experiment.Sample;
import com.szmolke.loadbalancingsim.simulation.SimulationCreator;
import com.szmolke.loadbalancingsim.stats.*;
import com.szmolke.loadbalancingsim.stats.impl.CloudletCost;
import com.szmolke.loadbalancingsim.stats.impl.CloudletWaitingTime;
import com.szmolke.loadbalancingsim.stats.impl.VmUsage;
import com.szmolke.loadbalancingsim.utils.ChartGenerator;
import com.szmolke.loadbalancingsim.utils.ChartManager;
import javafx.application.Application;
import javafx.stage.Stage;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.distributions.ContinuousDistribution;
import org.cloudbus.cloudsim.distributions.LognormalDistr;
import org.cloudbus.cloudsim.distributions.ParetoDistr;
import org.cloudbus.cloudsim.util.Log;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModel;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModelFull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The Master Thesis load balancing simulator.
 * Verify multiple load balancing algorithms which implements {@link LoadBalancer} interface.
 * The performance of algorithms was measured with metrics described in {@link StatsGenerator}
 *
 * @author lszmolke
 */
public class LoadBalancingSimulator extends Application {

    /**
     * Specifies if charts should be generated
     */
    public static final boolean GENERATE_INDIVIDUAL_CHARTS = true;
    public static final boolean GENERATE_CUMULATIVE_CHARTS = true;
    public static final boolean DELETE_OLD_CHARTS = true;

    /* UTILIZATION PARAMS - START */
    public static final Class<? extends UtilizationModel> UTILIZATION_BW = UtilizationModelFull.class;
    public static final Class<? extends UtilizationModel> UTILIZATION_CPU = UtilizationModelFull.class;
    public static final Class<? extends UtilizationModel> UTILIZATION_RAM = UtilizationModelFull.class;
    /* UTILIZATION PARAMS - END */
    /**
     * Contains matlab functions to hyphothesis testing
     */

    public static final boolean LOG_CLOUDSIM = false;
    public static final boolean LOG_CUSTOM = true;
    public static final int EXPERIMENTS_SIZE = 10;
    public static final int CLOUDLETS_COUNT = 50;

    /**
     * Load balancers which will be tested by the application.
     */
    private static final List<Class<? extends LoadBalancer>> LBS_FOR_TESTING = List.of(
            RoundRobin.class,
            WeightedRoundRobin.class,
            Random.class,
            LeastLoaded.class,
            LeastConnections.class,
            DynamicRouletteSelection.class
    );


    CloudletsCreator cloudletsCreator = CloudletsCreator.getInstance();
    boolean simulationParamsPrinted = false;

    @Override
    public void start(Stage stage) throws Exception {
        if (DELETE_OLD_CHARTS) {
            ChartManager.deleteCharts();
        }

        List<Sample> samples = new ArrayList<>(); // EXPERIMENTS_SIZE * LBS_FOR_TESTING_SIZE
        for (int sampleId = 0; sampleId < EXPERIMENTS_SIZE * LBS_FOR_TESTING.size(); ) {
            //Map<Class<? extends LoadBalancer>, StatsGenerator> statsGeneratorMap = new LinkedHashMap<>();

            TestedLoadBalancersStats testedLoadBalancersStats = new TestedLoadBalancersStats(sampleId / LBS_FOR_TESTING.size());
            int experimentNumber = sampleId / LBS_FOR_TESTING.size();

            //Iterate over load balancers
            List<Cloudlet> toSend = null;

            ContinuousDistribution distribution = new ParetoDistr(1.133, 0.30);
            // create delays
            List<Double> delays = IntStream.range(0, CLOUDLETS_COUNT).mapToObj(i -> distribution.sample() * CloudletsDistributor.DISTRIBUTION_MULTIPLIER).collect(Collectors.toList());

            for (int i = 0; i < LBS_FOR_TESTING.size(); i++) {
                Class<? extends LoadBalancer> testedLb = LBS_FOR_TESTING.get(i);
                System.out.println("START experiment. SampleId = " + sampleId + ". LoadBalancer: " + testedLb.getSimpleName() + ". Rest samples to finish: " + (EXPERIMENTS_SIZE * LBS_FOR_TESTING.size() - (sampleId + 1)));
                // Create cloudlets to send

                toSend = i == 0 ? cloudletsCreator.createCloudlets(0, CLOUDLETS_COUNT) : cloudletsCreator.createCopy(toSend);
                //Cloudlets distribution kind

                StatsGenerator statsGenerator = this.startExperiment(experimentNumber, stage, testedLb, toSend, delays, GENERATE_INDIVIDUAL_CHARTS);
                samples.add(new Sample(sampleId, testedLb, statsGenerator));
                sampleId++;
                testedLoadBalancersStats.put(statsGenerator);
            }
            ChartGenerator chartGenerator = new ChartGenerator(testedLoadBalancersStats, experimentNumber);
            chartGenerator.generateCharts();

            testedLoadBalancersStats.createExcel(experimentNumber, VmUsage.VM_USAGE_STATS_ID);
            testedLoadBalancersStats.createExcel(experimentNumber, CloudletWaitingTime.RESPONSE_TIME_STATS_ID);
            testedLoadBalancersStats.createExcel(experimentNumber, CloudletCost.CLOUDLET_COST_STATS_ID);
        }
        try {
            ExperimentsEvaluator experimentsEvaluator = ExperimentsEvaluator.getInstance();
            experimentsEvaluator.evaluateAverageExecutionTime(samples);
            experimentsEvaluator.evaluateMedianExecutionTime(samples);
            experimentsEvaluator.evaluateAverageWaitingTime(samples);
            experimentsEvaluator.evaluateAveragTotalCost(samples);
            experimentsEvaluator.showDrops(samples);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("FINITO");
        System.exit(0);
    }

    public StatsGenerator startExperiment(int experimentNumber, Stage stage, Class<? extends LoadBalancer> loadBalancer, List<Cloudlet> toSend, List<Double> delays, boolean generateCharts) {
        // Create simulation
        SimulationCreator simulation = new SimulationCreator(loadBalancer);
        if (LOG_CUSTOM && !simulationParamsPrinted) {
            simulation.printParams();
            simulationParamsPrinted = true;
        }

        if (!LOG_CLOUDSIM) {
            Log.disable();
        }

        // Create obj to collect statistics
        StatsGenerator statsGenerator = new StatsGenerator(simulation.getBroker(), loadBalancer);
        Map<Integer, Stats> statsCollector = statsGenerator.getStatsCollector();
        applyListeners(cloudletsCreator, statsCollector);

        // Distribute cloudlets
        CloudletsDistributor distributor = new CloudletsDistributor(simulation.getBroker());
        // Send cloudlets
        distributor.send(toSend);

        // Start simulation
        simulation.start();

        // Print result
        if (LOG_CUSTOM) {
            statsGenerator.printCollectedStatistics(loadBalancer);
        }

        if (generateCharts) {
            // Generate charts
            List<Stats> stats = statsCollector.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toList());
            ChartGenerator chartGenerator = new ChartGenerator(loadBalancer, stage, stats, experimentNumber);
            chartGenerator.generateCharts();
        }

        return statsGenerator;
    }


    /**
     * Apply callbacks from Cloudlets to Statistics accumulator
     */
    private void applyListeners(CloudletsCreator cloudletsCreator, Map<Integer, Stats> statsCollector) {
        List<StatsMonitorFinishCallback> finishCallbacks = statsCollector.values().stream()
                .filter(value -> value.callbackMode().equals(Stats.CallbackMode.FINISH))
                .map(stats -> {
                    StatsMonitorFinishCallback onModifyCloudlet = stats::onModifyCloudlet;
                    return onModifyCloudlet;
                }).collect(Collectors.toList());

        List<StatsMonitorUpdateCallback> updateCallbacks = statsCollector.values().stream()
                .filter(value -> value.callbackMode().equals(Stats.CallbackMode.UPDATE))
                .map(stats -> {
                    StatsMonitorUpdateCallback onModifyCloudlet = stats::onModifyCloudlet;
                    return onModifyCloudlet;
                }).collect(Collectors.toList());

        cloudletsCreator.setOnFinishCloudletsCallbacks(finishCallbacks);
        cloudletsCreator.setOnUpdateCloudletsCallbacks(updateCallbacks);
    }
}

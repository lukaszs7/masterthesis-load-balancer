package com.szmolke.loadbalancingsim.simulation;

import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import org.cloudbus.cloudsim.allocationpolicies.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.brokers.DatacenterBroker;
import org.cloudbus.cloudsim.brokers.DatacenterBrokerSimple;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.cloudlets.network.NetworkCloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.datacenters.Datacenter;
import org.cloudbus.cloudsim.datacenters.DatacenterCharacteristics;
import org.cloudbus.cloudsim.datacenters.DatacenterCharacteristicsSimple;
import org.cloudbus.cloudsim.datacenters.DatacenterSimple;
import org.cloudbus.cloudsim.datacenters.network.NetworkDatacenter;
import org.cloudbus.cloudsim.hosts.Host;
import org.cloudbus.cloudsim.hosts.HostSimple;
import org.cloudbus.cloudsim.hosts.network.NetworkHost;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.ResourceProvisionerSimple;
import org.cloudbus.cloudsim.resources.Pe;
import org.cloudbus.cloudsim.resources.PeSimple;
import org.cloudbus.cloudsim.schedulers.cloudlet.CloudletSchedulerCompletelyFair;
import org.cloudbus.cloudsim.schedulers.cloudlet.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.schedulers.vm.VmSchedulerSpaceShared;
import org.cloudbus.cloudsim.util.Log;
import org.cloudbus.cloudsim.vms.Vm;
import org.cloudbus.cloudsim.vms.VmSimple;
import org.cloudbus.cloudsim.vms.network.NetworkVm;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lszmolke
 */
public class SimulationCreator {
    /**
     * Simulation params
     */
    private static final int HOSTS = 1;
    private static final int VMS = 2;

    /**
     * Host params
     */
    private static final int HOST_MIPS = 100000;
    private static final int HOST_PES = 10;
    private static final int HOST_RAM = 20480;
    private static final int HOST_BW = 100000;
    private static final int HOST_STORAGE = 200000;

    /**
     * Vm params
     */
    private int VM_MIPS = 1000;
    private int VM_PES = 1;
    private int VM_RAM = 512;
    private int VM_BW = 1000;
    private int VM_SIZE = 10000;


    private final List<NetworkHost> hostList;
    private final List<NetworkVm> vmList;
    private final List<NetworkCloudlet> cloudletList;
    private final DatacenterBroker broker;
    private final NetworkDatacenter datacenter;
    private final CloudSim simulation;

    public SimulationCreator(Class<? extends LoadBalancer> loadBalancerKey) {
        simulation = new CloudSim();

        this.hostList = new ArrayList<>();
        this.cloudletList = new ArrayList<>();
        this.datacenter = createDatacenter();
        this.broker = new DatacenterBrokerSimple(simulation);
        this.vmList = createAndSubmitVms();

        LoadBalancer loadBalancer = (LoadBalancer) this.createLoadBalancer(loadBalancerKey);
        this.broker.setVmMapper(loadBalancer::balance);
    }

    public double start() {
        return this.simulation.start();
    }

    private List<NetworkVm> createAndSubmitVms() {
        final List<NetworkVm> list = new ArrayList<>(VMS);

        list.add(createVm(1, 2000, 2, 512, 1500, 1000));
        list.add(createVm(2, 2000, 2, 512, 1500, 1000));
        list.add(createVm(3, 2000, 2, 512, 1500, 1000));
        list.add(createVm(4, 2000, 2, 512, 1500, 1000));

        broker.submitVmList(list);

        return list;
    }

    private NetworkDatacenter createDatacenter() {
        for (int i = 0; i < HOSTS; i++) {
            hostList.add(createHost(i));
        }

        NetworkDatacenter datacenterSimple = new NetworkDatacenter(simulation, hostList, new VmAllocationPolicySimple());

        datacenterSimple.getCharacteristics()
                .setCostPerSecond(3.0)
                .setCostPerMem(0.05)
                .setCostPerStorage(0.1)
                .setCostPerBw(0.1);

        return datacenterSimple;
    }

    private NetworkHost createHost(int id) {
        List<Pe> peList = new ArrayList<>();
        long mips = HOST_MIPS;
        for (int i = 0; i < HOST_PES; i++) {
            peList.add(new PeSimple(mips, new PeProvisionerSimple()));
        }
        long ram = HOST_RAM; // host memory (MEGABYTE)
        long storage = HOST_STORAGE; // host storage (MEGABYTE)
        long bw = HOST_BW; //Megabits/s

        NetworkHost networkHost = new NetworkHost(ram, bw, storage, peList);
        networkHost.setRamProvisioner(new ResourceProvisionerSimple())
                .setBwProvisioner(new ResourceProvisionerSimple())
                .setVmScheduler(new VmSchedulerSpaceShared());
        return networkHost;
    }

    private NetworkVm createVm(int id, int mips, int pes, int ram, int bw, int size) {
        NetworkVm vm = new NetworkVm(id, mips, pes);
        vm
                .setRam(ram)
                .setBw(bw)
                .setSize(size)
                .setCloudletScheduler(new CloudletSchedulerCompletelyFair());
        return vm;
    }

    private Object createLoadBalancer(Class<? extends LoadBalancer> loadBalancer) {
        try {
            Class<?> clazz = Class.forName(loadBalancer.getName());
            Constructor<?> ctor = clazz.getConstructor(List.class, CloudSim.class);
            return ctor.newInstance(this.vmList, this.simulation);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public DatacenterBroker getBroker() {
        return broker;
    }

    public CloudSim getSimulation() {
        return simulation;
    }

    public void printParams() {
        System.out.println("------------------- PARAMETRY SYMULACJI -------------------");
        List<Vm> vmCreatedList = this.getBroker().getVmWaitingList();

        System.out.println("Liczba VM: "+vmCreatedList.size());
        System.out.println("Parametry VM:");
        for (int i = 0; i < vmCreatedList.size(); i++) {
            Vm vm = vmCreatedList.get(i);
            System.out.println("ID = "+vm.getId()+", MIPS: "+vm.getMips()+", PES: "+vm.getNumberOfPes()+", RAM: "+vm.getRam().getCapacity()+", BW: "+vm.getBw().getCapacity()+", SIZE: "+vm.getStorage().getCapacity());
        }
    }
}

package com.szmolke.loadbalancingsim.distribution;

import com.szmolke.loadbalancingsim.LoadBalancingSimulator;
import com.szmolke.loadbalancingsim.stats.StatsMonitorFinishCallback;
import com.szmolke.loadbalancingsim.stats.StatsMonitorUpdateCallback;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.cloudlets.CloudletSimple;
import org.cloudbus.cloudsim.cloudlets.network.CloudletExecutionTask;
import org.cloudbus.cloudsim.cloudlets.network.NetworkCloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.distributions.ContinuousDistribution;
import org.cloudbus.cloudsim.distributions.NormalDistr;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModel;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModelDynamic;
import org.cloudsimplus.listeners.CloudletVmEventInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author lszmolke
 */
public class CloudletsCreator {

    private List<StatsMonitorUpdateCallback> onUpdateCloudletsCallbacks;
    private List<StatsMonitorFinishCallback> onFinishCloudletsCallbacks;
    private CloudSim simulation;

    public void setSimulation(CloudSim simulation) {
        this.simulation = simulation;
    }

    private static class SingletonHelper {
        private static final CloudletsCreator INSTANCE = new CloudletsCreator();
    }

    public static CloudletsCreator getInstance() {
        return SingletonHelper.INSTANCE;
    }

    /**
     * Cloudlets params
     */
    private static final int FILE_SIZE = 10240;
    private static final int OUTPUT_SIZE = 20480;
    private static final int LENGTH = 10;
    private static final int RAM = 2048;
    private static final int PES = 2;


    private static final int EXECUTION_TASKS = 20;

    public List<Cloudlet> createCloudlets(int fromIndex, int count) {
        return IntStream.range(fromIndex, fromIndex + count)
                .mapToObj(i -> this.createCloudlet(i))
                .collect(Collectors.toList());
    }

    public List<Cloudlet> createCopy(List<Cloudlet> source) {
        if (source != null) {
            List<Cloudlet> newList = new ArrayList<>(source.size());
            for (int i = 0; i < source.size(); i++) {
                Cloudlet sc = source.get(i);
                Cloudlet newOne = this.createCloudlet(sc.getId(), sc.getLength(), sc.getNumberOfPes(), sc.getFileSize(), sc.getOutputSize());
                newList.add(newOne);
            }
            return newList;
        } else {
            return null;
        }
    }


    private Function<UtilizationModelDynamic, Double> utilizationModelDynamicCPU() {
        return (utilization) -> {
            return utilization.getUtilization();
        };
    }

    private Function<UtilizationModelDynamic, Double> utilizationModelDynamicBW() {
        return (utilization) -> {
            return utilization.getUtilization();
        };
    }

    private Function<UtilizationModelDynamic, Double> utilizationModelDynamicRAM() {
        return (utilization) -> {
            return utilization.getUtilization();
        };
    }

    public Cloudlet createCloudlet(int id) {
        int length = (int) (LENGTH * Math.random()) + 1;
        int pes = (int) (PES * Math.random()) + 1;


        int fileSize = (int) (FILE_SIZE * Math.random()) + 1;
        int outputSize = (int) (OUTPUT_SIZE * Math.random()) + 1;

        Cloudlet cloudlet = createCloudlet(id, length, pes, fileSize, outputSize);
        return cloudlet;
    }

    public Cloudlet createCloudlet(int id, long length, long pes, long fileSize, long outputSize) {
        CloudletSimple cloudlet = new CloudletSimple(id, length, pes);
        cloudlet.setFileSize(fileSize)
                .setOutputSize(outputSize)
                .setLength(length)
                .setUtilizationModelCpu(this.utilizationModel(LoadBalancingSimulator.UTILIZATION_CPU, utilizationModelDynamicCPU()))
                .setUtilizationModelBw(this.utilizationModel(LoadBalancingSimulator.UTILIZATION_BW, utilizationModelDynamicBW()))
                .setUtilizationModelRam(this.utilizationModel(LoadBalancingSimulator.UTILIZATION_RAM, utilizationModelDynamicRAM()))
                .addOnFinishListener(this::applyFinishCallback)
                .addOnUpdateProcessingListener(this::applyUpdateCallback);
        return cloudlet;
    }

    public CloudletExecutionTask createExecutionTask(int id, int lenght) {
        return new CloudletExecutionTask(id, lenght);
    }

    public UtilizationModel utilizationModel(Class<? extends UtilizationModel> clazz, Function<UtilizationModelDynamic, Double> utilizationModelFunction) {
        UtilizationModel utilizationModel = null;
        try {
            utilizationModel = clazz.getConstructor().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (clazz == UtilizationModelDynamic.class) {
            UtilizationModelDynamic dynamic = (UtilizationModelDynamic) utilizationModel;
            dynamic.setUtilizationUpdateFunction(utilizationModelFunction);
        }
        return utilizationModel;
    }

    public void setOnUpdateCloudletsCallbacks(List<StatsMonitorUpdateCallback> onUpdateCloudletsCallbacks) {
        this.onUpdateCloudletsCallbacks = onUpdateCloudletsCallbacks;
    }

    public void setOnFinishCloudletsCallbacks(List<StatsMonitorFinishCallback> onFinishCloudletsCallbacks) {
        this.onFinishCloudletsCallbacks = onFinishCloudletsCallbacks;
    }

    private void applyFinishCallback(CloudletVmEventInfo cloudletVmEventInfo) {
        this.onFinishCloudletsCallbacks.forEach(callback -> callback.apply(cloudletVmEventInfo));
    }

    private void applyUpdateCallback(CloudletVmEventInfo cloudletVmEventInfo) {
        this.onUpdateCloudletsCallbacks.forEach(callback -> callback.apply(cloudletVmEventInfo));
    }
}

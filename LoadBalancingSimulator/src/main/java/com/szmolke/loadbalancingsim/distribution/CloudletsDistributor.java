package com.szmolke.loadbalancingsim.distribution;

import org.cloudbus.cloudsim.brokers.DatacenterBroker;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.cloudlets.network.NetworkCloudlet;
import org.cloudbus.cloudsim.distributions.ContinuousDistribution;
import org.cloudbus.cloudsim.distributions.NormalDistr;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lszmolke
 */
public class CloudletsDistributor {
    /**
     * Distribution params
     */
    private static final int MEAN = 5;
    private static final int STANDARD_DEVIATION = 5;
    public static final int DISTRIBUTION_MULTIPLIER = 10;

    private ContinuousDistribution distributionKind;
    private DatacenterBroker recipient;

    private Map<Integer, Cloudlet> sendedCloudlets;
    private List<Double> delays;

    public CloudletsDistributor(DatacenterBroker recipient) {
        if (recipient == null) {
            throw new IllegalArgumentException("Params was not initialized properly");
        }

        this.recipient = recipient;
        this.distributionKind = new NormalDistr(MEAN, STANDARD_DEVIATION);
    }

    public CloudletsDistributor(DatacenterBroker recipient, ContinuousDistribution distributionKind) {
        this(recipient);
        this.distributionKind = distributionKind;
    }

    public CloudletsDistributor(DatacenterBroker recipient, List<Double> delays) {
        this(recipient);
        this.delays = delays;
    }

    public void send(List<Cloudlet> toSend) {
        sendedCloudlets = new HashMap<>(toSend.size());
        this.modifyDelay(toSend);

        sendRecursive(toSend, 0, toSend.size());
    }

    private void sendRecursive(List<Cloudlet> toSend, int actualIndex, int maxIndex) {
        if (actualIndex < maxIndex && !this.sendedCloudlets.containsKey(actualIndex)) {
            Cloudlet actualToSend = toSend.get(actualIndex);
//            actualToSend.addOnFinishListener(cloudletVmEventInfo -> {
                sendRecursive(toSend, actualIndex + 1, maxIndex);
//            });
            this.recipient.submitCloudlet(actualToSend);
            this.sendedCloudlets.put(actualIndex, actualToSend);
        }
    }

    private void modifyDelay(List<Cloudlet> toSend) {
        if(delays != null) {
            for (int i = 0; i < toSend.size(); i++) {
                toSend.get(i).setSubmissionDelay(delays.get(i));
            }
        } else {
            toSend.stream()
                    .forEach(cloudlet -> cloudlet.setSubmissionDelay(distributionKind.sample() * DISTRIBUTION_MULTIPLIER));
        }
    }
}

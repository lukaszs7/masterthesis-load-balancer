package com.szmolke.loadbalancingsim.experiment;

import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import com.szmolke.loadbalancingsim.stats.StatsGenerator;

import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

/**
 * @author lszmolke
 */
public class SampleUtils {
    private static class SingletonHelper {
        private static SampleUtils INSTANCE = new SampleUtils();
    }

    public static SampleUtils getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public List<Sample> filterByLoadBalancer(List<Sample> samples, Class<? extends LoadBalancer> loadBalancer) {
        return samples.stream()
                .filter(sample -> sample.getLoadBalancer() == loadBalancer)
                .collect(Collectors.toList());
    }

    public double[] collectSamplesDistibution(List<Sample> samples, Class<? extends LoadBalancer> loadBalancer, ToDoubleFunction<StatsGenerator> collectorFunction) {
        List<Sample> filtered = this.filterByLoadBalancer(samples, loadBalancer);
        return filtered.stream()
                .map(Sample::getStatsGenerator)
                .mapToDouble(collectorFunction)
                .toArray();
    }
}

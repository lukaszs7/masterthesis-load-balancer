package com.szmolke.loadbalancingsim.experiment;

import com.szmolke.loadbalancingsim.algorithm.impl.*;
import com.szmolke.loadbalancingsim.matlab.StatisticalHypothesisTester;
import com.szmolke.loadbalancingsim.matlab.TtestTail;
import com.szmolke.loadbalancingsim.stats.StatsGenerator;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudsimplus.builders.tables.CloudletsTableBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

/**
 * @author lszmolke
 */
public class ExperimentsEvaluator {

    private SampleUtils sampleUtils = SampleUtils.getInstance();
    private StatisticalHypothesisTester hypothesisTester = new StatisticalHypothesisTester();

    private static class SingletonHelper {
        private static final ExperimentsEvaluator INSTANCE = new ExperimentsEvaluator();
    }

    public static ExperimentsEvaluator getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public void evaluateAverageExecutionTime(List<Sample> samples) {
        System.out.println("START EVALUATING EXPERIMENTS RESULTS - AVERAGE EXECUTION TIME");
        hypothesisTester.connect();

        // Average execution time
        double[] rrAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, RoundRobin.class, StatsGenerator::cloudletsAverageExecutionTime);
        double[] wrrAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, WeightedRoundRobin.class, StatsGenerator::cloudletsAverageExecutionTime);
        double[] randAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, Random.class, StatsGenerator::cloudletsAverageExecutionTime);
        double[] lcAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, LeastConnections.class, StatsGenerator::cloudletsAverageExecutionTime);
        double[] luAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, LeastLoaded.class, StatsGenerator::cloudletsAverageExecutionTime);
        double[] drsAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, DynamicRouletteSelection.class, StatsGenerator::cloudletsAverageExecutionTime);
        this.printDistributions(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        this.checkSamplesNormalDistribution(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        // Check variances (is difference insignificant)
        double[][] distrs = new double[][] {rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr};

        // Check hyphothesis
        this.checkHyphothesis(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        hypothesisTester.disconnect();
    }

    public void evaluateMedianExecutionTime(List<Sample> samples) {
        System.out.println("START EVALUATING EXPERIMENTS RESULTS - MEDIAN EXECUTION TIME");
        hypothesisTester.connect();

        // Average execution time
        double[] rrAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, RoundRobin.class, StatsGenerator::cloudletsMedianExecutionTime);
        double[] wrrAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, WeightedRoundRobin.class, StatsGenerator::cloudletsMedianExecutionTime);
        double[] randAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, Random.class, StatsGenerator::cloudletsMedianExecutionTime);
        double[] lcAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, LeastConnections.class, StatsGenerator::cloudletsMedianExecutionTime);
        double[] luAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, LeastLoaded.class, StatsGenerator::cloudletsMedianExecutionTime);
        double[] drsAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, DynamicRouletteSelection.class, StatsGenerator::cloudletsMedianExecutionTime);
        this.printDistributions(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        this.checkSamplesNormalDistribution(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        // Check variances (is difference insignificant)
        double[][] distrs = new double[][] {rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr};
        //System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(distrs));

        // Check hyphothesis
        this.checkHyphothesis(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        hypothesisTester.disconnect();
    }

    private void checkHyphothesis(double[] rrDistr, double[] WRRDistr, double[] randDistr, double[] lcDistr, double[] luDistr, double[] DRSDistr) {
        List<String> answers = new ArrayList<>();
        // LU
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {luDistr, rrDistr}));
        answers.add(hypothesisTester.evalHyphothesis(luDistr, rrDistr, "LU < RR", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {luDistr, lcDistr}));
        answers.add(hypothesisTester.evalHyphothesis(luDistr, lcDistr, "LU < LC", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {luDistr, randDistr}));
        answers.add(hypothesisTester.evalHyphothesis(luDistr, randDistr, "LU < RAND", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {luDistr, DRSDistr}));
        answers.add(hypothesisTester.evalHyphothesis(luDistr, DRSDistr, "LU < DRS", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {luDistr, WRRDistr}));
        answers.add(hypothesisTester.evalHyphothesis(luDistr, WRRDistr, "LU < WRR", TtestTail.LEFT));

        // LC
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {lcDistr, rrDistr}));
        answers.add(hypothesisTester.evalHyphothesis(lcDistr, rrDistr, "LC < RR", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {lcDistr, luDistr}));
        answers.add(hypothesisTester.evalHyphothesis(lcDistr, luDistr, "LC < LU", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {lcDistr, randDistr}));
        answers.add(hypothesisTester.evalHyphothesis(lcDistr, randDistr, "LC < RAND", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {lcDistr, DRSDistr}));
        answers.add(hypothesisTester.evalHyphothesis(lcDistr, DRSDistr, "LC < DRS", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {lcDistr, WRRDistr}));
        answers.add(hypothesisTester.evalHyphothesis(lcDistr, WRRDistr, "LC < WRR", TtestTail.LEFT));

        // RR
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {rrDistr, luDistr}));
        answers.add(hypothesisTester.evalHyphothesis(rrDistr, luDistr, "RR < LU", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {rrDistr, lcDistr}));
        answers.add(hypothesisTester.evalHyphothesis(rrDistr, lcDistr, "RR < LC", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {rrDistr, randDistr}));
        answers.add(hypothesisTester.evalHyphothesis(rrDistr, randDistr, "RR < RAND", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {rrDistr, DRSDistr}));
        answers.add(hypothesisTester.evalHyphothesis(rrDistr, WRRDistr, "RR < WRR", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {rrDistr, WRRDistr}));
        answers.add(hypothesisTester.evalHyphothesis(rrDistr, DRSDistr, "RR < DRS", TtestTail.LEFT));

        // WRR
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {WRRDistr, rrDistr}));
        answers.add(hypothesisTester.evalHyphothesis(WRRDistr, rrDistr, "WRR < RR", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {WRRDistr, luDistr}));
        answers.add(hypothesisTester.evalHyphothesis(WRRDistr, luDistr, "WRR < LU", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {WRRDistr, lcDistr}));
        answers.add(hypothesisTester.evalHyphothesis(WRRDistr, lcDistr, "WRR < LC", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {WRRDistr, randDistr}));
        answers.add(hypothesisTester.evalHyphothesis(WRRDistr, randDistr, "WRR < RAND", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {WRRDistr, DRSDistr}));
        answers.add(hypothesisTester.evalHyphothesis(WRRDistr, DRSDistr, "WRR < DRS", TtestTail.LEFT));

        // RAND
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {randDistr, rrDistr}));
        answers.add(hypothesisTester.evalHyphothesis(randDistr, rrDistr, "R < RR", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {randDistr, WRRDistr}));
        answers.add(hypothesisTester.evalHyphothesis(randDistr, WRRDistr, "R < WRR", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {randDistr, lcDistr}));
        answers.add(hypothesisTester.evalHyphothesis(randDistr, lcDistr, "R < LC", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {randDistr, luDistr}));
        answers.add(hypothesisTester.evalHyphothesis(randDistr, luDistr, "R < LU", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {randDistr, DRSDistr}));
        answers.add(hypothesisTester.evalHyphothesis(randDistr, DRSDistr, "R < DRS", TtestTail.LEFT));

        // DRS
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {DRSDistr, rrDistr}));
        answers.add(hypothesisTester.evalHyphothesis(DRSDistr, rrDistr, "DRS < RR", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {DRSDistr, randDistr}));
        answers.add(hypothesisTester.evalHyphothesis(DRSDistr, randDistr, "DRS < RAND", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {DRSDistr, luDistr}));
        answers.add(hypothesisTester.evalHyphothesis(DRSDistr, luDistr, "DRS < LU", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {DRSDistr, lcDistr}));
        answers.add(hypothesisTester.evalHyphothesis(DRSDistr, lcDistr, "DRS < LC", TtestTail.LEFT));
        System.out.println("Is difference in variances insignificant: "+this.hypothesisTester.isDistrsVarianceDiffInsignificant(new double[][] {DRSDistr, WRRDistr}));
        answers.add(hypothesisTester.evalHyphothesis(DRSDistr, WRRDistr, "DRS < WRR", TtestTail.LEFT));

        System.out.println("------------- STATISTICAL ANSWERS -------------");
        for (int i = 0; i < answers.size(); i++) {
            String ans = answers.get(i);
            if(!"".equals(ans) && ans != null) {
                System.out.print(ans + ", ");
            }
        }
        System.out.println();
    }

    public void evaluateAverageWaitingTime(List<Sample> samples) {
        System.out.println("START EVALUATING EXPERIMENTS RESULTS - AVERAGE WAITING TIME");
        hypothesisTester.connect();

        // Average execution time
        double[] rrAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, RoundRobin.class, StatsGenerator::cloudletsAverageWaitingTime);
        double[] wrrAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, WeightedRoundRobin.class, StatsGenerator::cloudletsAverageWaitingTime);
        double[] randAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, Random.class, StatsGenerator::cloudletsAverageWaitingTime);
        double[] lcAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, LeastConnections.class, StatsGenerator::cloudletsAverageWaitingTime);
        double[] luAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, LeastLoaded.class, StatsGenerator::cloudletsAverageWaitingTime);
        double[] drsAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, DynamicRouletteSelection.class, StatsGenerator::cloudletsAverageWaitingTime);
        this.printDistributions(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        this.checkSamplesNormalDistribution(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        // Check variances (is difference insignificant)
        double[][] distrs = new double[][] {rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr};


        // Check hyphothesis
        this.checkHyphothesis(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);
        hypothesisTester.disconnect();
    }

    public void evaluateAveragTotalCost(List<Sample> samples) {
        System.out.println("START EVALUATING EXPERIMENTS RESULTS - AVERAGE TOTAL COST");
        hypothesisTester.connect();

        // Average execution time
        double[] rrAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, RoundRobin.class, StatsGenerator::cloudletsAverageTotalCost);
        double[] wrrAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, WeightedRoundRobin.class, StatsGenerator::cloudletsAverageTotalCost);
        double[] randAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, Random.class, StatsGenerator::cloudletsAverageTotalCost);
        double[] lcAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, LeastConnections.class, StatsGenerator::cloudletsAverageTotalCost);
        double[] luAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, LeastLoaded.class, StatsGenerator::cloudletsAverageTotalCost);
        double[] drsAvgExecTimeDistr = sampleUtils.collectSamplesDistibution(samples, DynamicRouletteSelection.class, StatsGenerator::cloudletsAverageTotalCost);
        this.printDistributions(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        this.checkSamplesNormalDistribution(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);

        // Check variances (is difference insignificant)
        double[][] distrs = new double[][] {rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr};


        // Check hyphothesis
        this.checkHyphothesis(rrAvgExecTimeDistr, wrrAvgExecTimeDistr, randAvgExecTimeDistr, lcAvgExecTimeDistr, luAvgExecTimeDistr, drsAvgExecTimeDistr);
        hypothesisTester.disconnect();
    }

    private void checkSamplesNormalDistribution(double[] rrAvgExecTimeDistr, double[] wrrAvgExecTimeDistr, double[] randAvgExecTimeDistr, double[] lcAvgExecTimeDistr, double[] luAvgExecTimeDistr, double[] drsAvgExecTimeDistr) {
        System.out.println("START - checking samples normal distribution");
        System.out.println("RoundRobin: " + hypothesisTester.isNormalDistribution(rrAvgExecTimeDistr) + ", WeightedRoundRobin: " + hypothesisTester.isNormalDistribution(wrrAvgExecTimeDistr) +
                ", Random: " + hypothesisTester.isNormalDistribution(randAvgExecTimeDistr) + ", LeastConnection: " + hypothesisTester.isNormalDistribution(lcAvgExecTimeDistr) + ", LeastLoaded: " +
                hypothesisTester.isNormalDistribution(luAvgExecTimeDistr)+", RouletteSelection: "+hypothesisTester.isNormalDistribution(drsAvgExecTimeDistr));
    }

    private void printDistributions(double[]... distibutions) {
        for (int i = 0; i < distibutions.length; i++) {
            double[] distibution = distibutions[i];
            List<String> listDistr = DoubleStream.of(distibution).boxed().map(String::valueOf).collect(Collectors.toList());
            System.out.println("Distribution index: "+i+" : ["+String.join(",", listDistr)+"]");
        }
    }

    public void showDrops(List<Sample> samples) {
        System.out.println("SHOW DROPS");
        samples.stream()
                .map(sample -> sample.getStatsGenerator())
                .filter(statsGen -> statsGen.getBroker().getCloudletFinishedList().size() != statsGen.getBroker().getCloudletCreatedList().size())
                .forEach(statsGen -> {
                    System.out.println("DROPY DLA ALGORYTMU: "+statsGen.getTestedLb().getSimpleName());
                    List<Cloudlet> drops = statsGen.getBroker().getCloudletCreatedList().stream().filter(cloudlet -> cloudlet.getStatus() != Cloudlet.Status.SUCCESS).collect(Collectors.toList());
                    drops.forEach(cloudlet -> {
                        System.out.println("Cloudlet id = "+cloudlet.getId()+", Length = "+cloudlet.getLength()+", Status = "+cloudlet.getStatus().name());
                    });
                });
    }
}

package com.szmolke.loadbalancingsim.experiment;

import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import com.szmolke.loadbalancingsim.stats.StatsGenerator;

/**
 * Represents statistical sample
 * @author lszmolke
 */
public class Sample {

    private int id; // sample id
    private Class<? extends LoadBalancer> loadBalancer; // tested load balancer
    private StatsGenerator statsGenerator; // stats generated for sample

    public Sample(int id, Class<? extends LoadBalancer> loadBalancer, StatsGenerator statsGenerator) {
        this.id = id;
        this.loadBalancer = loadBalancer;
        this.statsGenerator = statsGenerator;
    }

    public int getId() {
        return id;
    }

    public Class<? extends LoadBalancer> getLoadBalancer() {
        return loadBalancer;
    }

    public StatsGenerator getStatsGenerator() {
        return statsGenerator;
    }
}

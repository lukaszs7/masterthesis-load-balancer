package com.szmolke.loadbalancingsim.algorithm.impl;

import com.szmolke.loadbalancingsim.algorithm.AbstractLoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.cloudlets.network.CloudletTask;
import org.cloudbus.cloudsim.cloudlets.network.NetworkCloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.datacenters.network.NetworkDatacenter;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.List;

/**
 * @author lszmolke
 */
public class Random extends AbstractLoadBalancer implements LoadBalancer {

    public Random(List<Vm> vms, CloudSim cloudSim) {
        super(vms, cloudSim);
    }

    @Override
    public Vm balance(Cloudlet cloudlet) {
        int randomIndex = (int)(Math.random() * vms.size());
        return vms.get(randomIndex);
    }
}

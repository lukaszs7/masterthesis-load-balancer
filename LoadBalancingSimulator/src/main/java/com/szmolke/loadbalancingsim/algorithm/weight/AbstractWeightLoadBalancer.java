package com.szmolke.loadbalancingsim.algorithm.weight;

import com.szmolke.loadbalancingsim.algorithm.AbstractLoadBalancer;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.List;

/**
 * @author lszmolke
 */
public abstract class AbstractWeightLoadBalancer extends AbstractLoadBalancer {
    protected VmWeightCreator vmWeightCreator;

    public AbstractWeightLoadBalancer(List<Vm> vms, CloudSim simulation) {
        super(vms, simulation);
        vmWeightCreator = new VmWeightCreator(vms, simulation);
    }

    protected abstract Vm chooseVm(VmWeightCreator vmWeightCreator);
}

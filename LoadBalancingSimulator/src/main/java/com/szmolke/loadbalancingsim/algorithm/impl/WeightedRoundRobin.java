package com.szmolke.loadbalancingsim.algorithm.impl;

import com.szmolke.loadbalancingsim.algorithm.AbstractLoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.weight.AbstractWeightLoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.weight.VmWeight;
import com.szmolke.loadbalancingsim.algorithm.weight.VmWeightCreator;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.List;

/**
 * @author lszmolke
 */
public class WeightedRoundRobin extends AbstractWeightLoadBalancer implements LoadBalancer {

    public WeightedRoundRobin(List<Vm> vms, CloudSim simulation) {
        super(vms, simulation);
    }
    private int lastSelectedVm = 0;
    @Override
    public Vm balance(Cloudlet cloudlet) {
        return this.chooseVm(vmWeightCreator);
    }


    protected Vm chooseVm(VmWeightCreator vmWeightCreator) {
        List<VmWeight> vmWeights = vmWeightCreator.getVmWeights();

        double actualWeight = 0;
        double nextRandomW = Double.MAX_VALUE;
        int nextIndex = 0;
        do {
            nextIndex = (lastSelectedVm + 1) % this.vms.size();
            nextRandomW = Math.random() * vmWeightCreator.getMaxWeight();
            lastSelectedVm = nextIndex;
            actualWeight = vmWeights.get(nextIndex).getWeight();
        } while (nextRandomW > actualWeight);

        // get last one
        return this.vms.get(nextIndex);
    }
}

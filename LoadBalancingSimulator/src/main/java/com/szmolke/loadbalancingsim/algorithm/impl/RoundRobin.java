package com.szmolke.loadbalancingsim.algorithm.impl;

import com.szmolke.loadbalancingsim.algorithm.AbstractLoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.List;

/**
 * @author lszmolke
 */
public class RoundRobin extends AbstractLoadBalancer implements LoadBalancer {

    public RoundRobin(List<Vm> vms, CloudSim cloudSim) {
        super(vms, cloudSim);
    }

    private int lastSelectedVm = 0;
    @Override
    public Vm balance(Cloudlet cloudlet) {
        int nextIndex = (lastSelectedVm + 1) % this.vms.size();
        lastSelectedVm = nextIndex;
        return this.vms.get(nextIndex);
    }

    private void printI(String s, double waitingTime) {
        if(waitingTime != 0) {
            System.out.println("TU: " + s + ": " + waitingTime);
        }
    }
}

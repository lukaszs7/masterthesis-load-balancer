package com.szmolke.loadbalancingsim.algorithm.impl;

import com.szmolke.loadbalancingsim.algorithm.AbstractLoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.HistoryEvent;
import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lszmolke
 */
public class LeastConnections extends AbstractLoadBalancer implements LoadBalancer {

    public LeastConnections(List<Vm> vms, CloudSim cloudSim) {
        super(vms, cloudSim);
    }

    @Override
    public Vm balance(Cloudlet cloudlet) {
        super.updateHistoryEvents(simulation.clock());
        Vm chosen = this.chooseVm(vms);
        super.addHistory(new HistoryEvent(simulation.clock(), chosen, cloudlet));
        return chosen;
    }

    private Vm chooseVm(List<Vm> createdVms) {
        return createdVms.stream()
                .sorted(Comparator.comparingDouble(vm -> this.findActualConnectionsOnVm(vm.getId())))
                .findFirst()
                .orElse(null);
    }

    protected int findActualConnectionsOnVm(int vmId) {
        return this.historyEvents.stream()
                .filter(he -> he.getVm().getId() == vmId)
                .collect(Collectors.toList()).size();
    }
}

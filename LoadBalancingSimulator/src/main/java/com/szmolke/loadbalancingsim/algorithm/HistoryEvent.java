package com.szmolke.loadbalancingsim.algorithm;

import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.vms.Vm;

/**
 * @author lszmolke
 */
public class HistoryEvent {
    private double time;
    private Vm vm;
    private Cloudlet cloudlet;

    public HistoryEvent(double time, Vm vm, Cloudlet cloudlet) {
        this.time = time;
        this.vm = vm;
        this.cloudlet = cloudlet;
    }

    public double getTime() {
        return time;
    }

    public Vm getVm() {
        return vm;
    }

    public Cloudlet getCloudlet() {
        return cloudlet;
    }
}


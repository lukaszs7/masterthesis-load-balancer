package com.szmolke.loadbalancingsim.algorithm.impl;

import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.weight.AbstractWeightLoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.weight.VmWeight;
import com.szmolke.loadbalancingsim.algorithm.weight.VmWeightCreator;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.List;

/**
 * @author lszmolke
 */
public class DynamicRouletteSelection extends AbstractWeightLoadBalancer implements LoadBalancer {

    public DynamicRouletteSelection(List<Vm> vms, CloudSim cloudSim) {
        super(vms, cloudSim);
    }

    @Override
    public Vm balance(Cloudlet cloudlet) {
        Vm vm = this.chooseVm(vmWeightCreator);
        return vm;
    }

    protected Vm chooseVm(VmWeightCreator vmWeightCreator) {
        List<VmWeight> vmWeights = vmWeightCreator.dynamicWeights(vms);
        double nextRandom = Math.random() * vmWeightCreator.dynamicWeightsSum(vmWeights);
        double actualWeight = 0;
        for (int i = 0; i < vmWeights.size(); i++) {
            actualWeight += vmWeights.get(i).getWeight();

            if(actualWeight > nextRandom) {
                return vmWeights.get(i).getVm();
            }
        }

        // get last one
        return vmWeights.get(vmWeights.size() - 1).getVm();
    }


}

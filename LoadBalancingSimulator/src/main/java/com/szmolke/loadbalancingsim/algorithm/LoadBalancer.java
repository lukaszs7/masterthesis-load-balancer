package com.szmolke.loadbalancingsim.algorithm;

import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.vms.Vm;

/**
 * @author lszmolke
 */
public interface LoadBalancer {
    Vm balance(Cloudlet cloudlet);
}

package com.szmolke.loadbalancingsim.algorithm.impl;

import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.weight.AbstractWeightLoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.weight.VmWeight;
import com.szmolke.loadbalancingsim.algorithm.weight.VmWeightCreator;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.List;

/**
 * @author lszmolke
 */
public class StaticRouletteSelection extends AbstractWeightLoadBalancer implements LoadBalancer {

    public StaticRouletteSelection(List<Vm> vms, CloudSim cloudSim) {
        super(vms, cloudSim);
    }

    @Override
    public Vm balance(Cloudlet cloudlet) {
        return this.chooseVm(vmWeightCreator);
    }


    protected Vm chooseVm(VmWeightCreator vmWeightCreator) {
        List<VmWeight> vmWeights = vmWeightCreator.getVmWeights();
        double nextRandom = Math.random() * vmWeightCreator.getWeightSum();
        double actualWeight = 0;
        for (int i = 0; i < vmWeights.size(); i++) {
            actualWeight += vmWeights.get(i).getWeight();

            if(actualWeight > nextRandom) {
                return vmWeights.get(i).getVm();
            }
        }

        // get last one
        return vmWeights.get(vmWeights.size() - 1).getVm();
    }
}

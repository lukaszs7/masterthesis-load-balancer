package com.szmolke.loadbalancingsim.algorithm.impl;

import com.szmolke.loadbalancingsim.algorithm.AbstractLoadBalancer;
import com.szmolke.loadbalancingsim.algorithm.HistoryEvent;
import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import org.apache.commons.math3.stat.StatUtils;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.vms.Vm;
import org.cloudbus.cloudsim.vms.network.NetworkVm;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lszmolke
 */
public class LeastLoaded extends AbstractLoadBalancer implements LoadBalancer {


    public LeastLoaded(List<Vm> vms, CloudSim cloudSim) {
        super(vms, cloudSim);
    }

    @Override
    public Vm balance(Cloudlet cloudlet) {
        super.updateHistoryEvents(simulation.clock());
        Vm chosen = this.chooseVm(vms);
        super.addHistory(new HistoryEvent(simulation.clock(), chosen, cloudlet));
        return chosen;
    }

    private Vm chooseVm(List<Vm> createdVms) {
        Vm toReturn = createdVms.stream()
                .sorted(Comparator.comparingDouble(vm -> this.findActualUsageOnVm(vm.getId())))
                .findFirst()
                .orElse(null);

        return toReturn;
    }

    protected double findActualUsageOnVm(int vmId) {
        return this.historyEvents.stream()
                .filter(he -> he.getVm().getId() == vmId)
                .mapToDouble(he -> this.calculateVmLoad(he.getCloudlet(), he.getVm()))
                .sum();

    }

    private double calculateVmLoad(Cloudlet cloudlet, Vm vm) {
        double cpuPercentUsage = cloudlet.getUtilizationOfCpu() * vm.getProcessor().getTotalMips();
        double allocatedBw = (cloudlet.getUtilizationOfBw() * cloudlet.getFileSize()) + (cloudlet.getUtilizationOfBw()*cloudlet.getOutputSize());
        double allocatedStorage = (vm.getStorage().getCapacity() - cloudlet.getFileSize());
        double allocatedRam = cloudlet.getUtilizationOfRam() * vm.getRam().getCapacity();

        double[] normalized = StatUtils.normalize(new double[] {cpuPercentUsage, allocatedBw, allocatedStorage, allocatedRam});

        return Math.abs(normalized[0]) + Math.abs(normalized[1]) + Math.abs(normalized[2]) + Math.abs(normalized[3]);
    }
}

package com.szmolke.loadbalancingsim.algorithm.weight;

import org.cloudbus.cloudsim.vms.Vm;

/**
 * @author lszmolke
 */
public class VmWeight {
    private Vm vm;
    private double weight;

    public VmWeight(Vm vm, double weight) {
        this.vm = vm;
        this.weight = weight;
    }

    public Vm getVm() {
        return vm;
    }

    public double getWeight() {
        return weight;
    }

    public void setVm(Vm vm) {
        this.vm = vm;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}

package com.szmolke.loadbalancingsim.algorithm;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.datacenters.Datacenter;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lszmolke
 */
public abstract class AbstractLoadBalancer {
    protected List<Vm> vms;
    protected CloudSim simulation;
    protected List<HistoryEvent> historyEvents;
    protected StandardDeviation std = new StandardDeviation();
    protected Mean mean = new Mean();

    public AbstractLoadBalancer(List<Vm> vms, CloudSim simulation) {
        this.vms = vms;
        this.simulation = simulation;
        this.historyEvents = new ArrayList<>();
    }

    public void addHistory(HistoryEvent history) {
        this.historyEvents.add(history);
    }

    public void updateHistoryEvents(final double clock) {
        // Remove finished history events
        this.historyEvents = this.historyEvents.stream()
                .filter(historyEvent -> historyEvent.getCloudlet().getFinishTime() == -1)
                .collect(Collectors.toList());
    }

    protected double zScoreNormalize(double x, double mean, double std) {
        // return ZScore normalize
        NormalDistribution normalDistribution = new NormalDistribution(mean, std);

        return normalDistribution.density(x);
    }

    protected double std(double[] inputs) {
        return this.std.evaluate(inputs);
    }

    protected double mean(double[] inputs, double[] weights) {
        return this.mean.evaluate(inputs, weights);
    }

    protected double zeroIfNan(double x) {
        return Double.isNaN(x) ? 0 : x;
    }
}


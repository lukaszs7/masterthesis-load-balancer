package com.szmolke.loadbalancingsim.algorithm.weight;

import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lszmolke
 */
public class VmWeightCreator {

    private static final double MIPS_WEIGHT = 15;
    private static final double RAM_WEIGHT = 5;
    private static final double BW_WEIGHT = 8;
    private static final double STORAGE_WEIGHT = 2;

    private final double sumMips;
    private final double sumRam;
    private final double sumBw;
    private final double sumStorage;
    private final List<VmWeight> vmWeights;
    private List<Vm> vms;

    private CloudSim simulation;

    public VmWeightCreator(List<Vm> vms, CloudSim simulation) {
        this.vms = vms;
        this.sumMips = vms.stream().mapToDouble(vm -> vm.getProcessor().getTotalMips()).sum();
        this.sumRam = vms.stream().mapToDouble(vm -> vm.getRam().getCapacity()).sum();
        this.sumBw = vms.stream().mapToDouble(vm -> vm.getBw().getCapacity()).sum();
        this.sumStorage = vms.stream().mapToDouble(vm -> vm.getStorage().getCapacity()).sum();
        this.vmWeights = createVmWeight(vms);
        this.simulation = simulation;
    }

    public List<VmWeight> dynamicWeights(List<Vm> vms) {
        return vms.stream()
                .map(vm -> new VmWeight(vm, computeDynamicWeight(vm)))
                .sorted((vm1, vm2) -> ((Double)vm1.getWeight()).compareTo(vm2.getWeight()))
                .collect(Collectors.toList());
    }

    public double dynamicWeightsSum(List<VmWeight> weights) {
        return weights.stream()
                .mapToDouble(VmWeight::getWeight)
                .sum();
    }

    private double computeDynamicWeight(Vm vm) {
        double mipsWeight = (vm.getProcessor().getTotalMips() - vm.getTotalCpuMipsUsage(simulation.clock())) * MIPS_WEIGHT;
        double ramWeight = (vm.getRam().getAvailableResource()) * RAM_WEIGHT;
        double bwWeight = (vm.getBw().getAvailableResource()) * BW_WEIGHT;
        double storageWeight = (vm.getStorage().getAvailableResource()) * STORAGE_WEIGHT;
        return mipsWeight + ramWeight + bwWeight + storageWeight;
    }

    public List<VmWeight> createVmWeight(List<Vm> vms) {
        return vms.stream()
                .map(vm -> new VmWeight(vm, computeWeight(vm)))
                .collect(Collectors.toList());
    }

    public double getWeightSum() {
        return vmWeights.stream()
                .mapToDouble(VmWeight::getWeight)
                .sum();
    }

    private double computeWeight(Vm vm) {
        return ((vm.getProcessor().getTotalMips() / sumMips) * MIPS_WEIGHT) +
                ((vm.getRam().getCapacity() / sumRam) * RAM_WEIGHT) +
                ((vm.getBw().getCapacity() / sumBw) * BW_WEIGHT) +
                ((vm.getStorage().getCapacity() / sumStorage) * STORAGE_WEIGHT);
    }

    public List<VmWeight> getVmWeights() {
        return vmWeights;
    }

    public double getMaxWeight() {
        return vmWeights.stream()
                .mapToDouble(vmWeight -> vmWeight.getWeight())
                .max()
                .getAsDouble();
    }
}

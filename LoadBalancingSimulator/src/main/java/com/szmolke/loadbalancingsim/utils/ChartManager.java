package com.szmolke.loadbalancingsim.utils;

import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import org.cloudbus.cloudsim.util.Log;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author lszmolke
 */
public class ChartManager {
    private static final String SAVE_ROOT_PATH = "E:\\Workspace intellij idea projects\\masterthesis-load-balancer\\LoadBalancingSimulator\\src\\main\\resources\\charts\\";

    public static void saveIndividualChart(Class<? extends LoadBalancer> testedLb, String folderName, WritableImage chart, String fileName, String extension) {
        if (chart == null || !isFilled(fileName) || !isFilled(extension)) {
            throw new IllegalArgumentException("All arguments should be passed.");
        }
        try {

            Path chartFolder = ChartManager.createDirIfNotExists(SAVE_ROOT_PATH);
            Path experimentDir = ChartManager.createDirIfNotExists(chartFolder.toAbsolutePath().toString() + "\\" + folderName);
            Path lbDir = ChartManager.createDirIfNotExists(experimentDir.toAbsolutePath().toString() + "\\" + testedLb.getSimpleName());

            String fullPathFile = lbDir.toAbsolutePath().toString() + "\\" + fileName + "." + extension;
            File file = new File(fullPathFile);

            ImageIO.write(SwingFXUtils.fromFXImage(chart, null), extension, file);
        } catch (IOException e) {
            Log.printLine("Error while generating chart file!");
            e.printStackTrace();
        }
        System.out.println("[INDIVIDUAL] Stop saving chart: " + fileName + "." + extension);
    }

    public static Path createDirIfNotExists(String path) throws IOException {
        Path dir = Paths.get(path);
        if (!dir.toFile().exists()) {
            Files.createDirectory(dir);
        }
        return dir;
    }

    public static void deleteCharts() {
        try {
            org.apache.commons.io.FileUtils.deleteDirectory(new File("E:\\Workspace intellij idea projects\\masterthesis-load-balancer\\LoadBalancingSimulator\\src\\main\\resources\\charts\\"));
            System.out.println("Successfully deleted all charts");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean isFilled(String str) {
        return str != null && str.trim().length() > 0;
    }

    public static void saveCumulativeChart(String folderName, WritableImage chart, String fileName, String extension) {
        if (chart == null || !isFilled(fileName) || !isFilled(extension)) {
            throw new IllegalArgumentException("All arguments should be passed.");
        }
        try {

            Path chartFolder = ChartManager.createDirIfNotExists(SAVE_ROOT_PATH);
            Path experimentDir = ChartManager.createDirIfNotExists(chartFolder.toAbsolutePath().toString() + "\\" + folderName);

            String fullPathFile = experimentDir.toAbsolutePath().toString() + "\\" + fileName + "." + extension;
            File file = new File(fullPathFile);

            ImageIO.write(SwingFXUtils.fromFXImage(chart, null), extension, file);
        } catch (IOException e) {
            Log.printLine("Error while generating chart file!");
            e.printStackTrace();
        }
        System.out.println("[CUMULATIVE] Stop saving chart: " + fileName + "." + extension);
    }
}

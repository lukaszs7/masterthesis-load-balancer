package com.szmolke.loadbalancingsim.utils;

import com.szmolke.loadbalancingsim.LoadBalancingSimulator;
import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import com.szmolke.loadbalancingsim.stats.Stats;
import com.szmolke.loadbalancingsim.stats.StatsGenerator;
import com.szmolke.loadbalancingsim.stats.TestedLoadBalancersStats;
import com.szmolke.loadbalancingsim.stats.dto.PropagationCloudletEntry;
import com.szmolke.loadbalancingsim.stats.dto.UsageEntry;
import com.szmolke.loadbalancingsim.stats.impl.*;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import org.cloudbus.cloudsim.util.Log;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lszmolke
 */
public class ChartGenerator {
    private Stage stage;
    private List<Stats> stats;
    private Class<? extends LoadBalancer> testedLb;
    private int experimentNumber;
    private TestedLoadBalancersStats testedLoadBalancersStats;

    public ChartGenerator(Stage stage, List<Stats> stats, int experimentNumber) {
        this.stage = stage;
        this.stats = stats;
        this.experimentNumber = experimentNumber;
    }

    public ChartGenerator(Class<? extends LoadBalancer> testedLb, Stage stage, List<Stats> stats, int experimentNumber) {
        this(stage, stats, experimentNumber);
        this.testedLb = testedLb;
    }

    public ChartGenerator(TestedLoadBalancersStats testedLoadBalancersStats, int experimentNumber) {
        this.testedLoadBalancersStats = testedLoadBalancersStats;
        this.experimentNumber = experimentNumber;
    }

    public void generateCharts() {
        if(testedLoadBalancersStats != null) {
            this.generateCumulativeStats(testedLoadBalancersStats);
        } else {
            this.stats.forEach(this::generateChartPerLb);
        }
    }

    private void generateCumulativeStats(TestedLoadBalancersStats testedLoadBalancersStats) {
        if(LoadBalancingSimulator.GENERATE_CUMULATIVE_CHARTS) {
            this.generateCumulativeStats(testedLoadBalancersStats.getCumulativeLBStats(), VmUsage.VM_USAGE_STATS_ID);
            this.generateCumulativeStats(testedLoadBalancersStats.getCumulativeLBStats(), CloudletWaitingTime.RESPONSE_TIME_STATS_ID);
            this.generateCumulativeStats(testedLoadBalancersStats.getCumulativeLBStats(), CloudletCost.CLOUDLET_COST_STATS_ID);
        }
    }

    private void generateCumulativeStats(List<StatsGenerator> cumulativeStats, int statsId) {
        String title = "";
        String xLabel = "";
        String yLabel = "";
        String fileName = "";
        ValueAxis xAxis = null;
        ValueAxis yAxis = null;

        switch (statsId) {
            case VmUsage.VM_USAGE_STATS_ID:
                title = "Statystyka zawierająca Zużycie maszyn wirtualnych w czasie - MIPS(t) dla testowanych algorytmów.";
                xLabel = "Czas [s]";
                yLabel = "Zużycie [MIPS]";
                fileName = "VmUsage";

                xAxis = new NumberAxis();
                xAxis.setMinorTickVisible(false);
                yAxis = new LogarithmicNumberAxis(1000, 400000);
                break;
            case CloudletWaitingTime.RESPONSE_TIME_STATS_ID:
                title = "Statystyka zawierająca czas oczekiwania [ms] w DataCenter dla Cloudletów.";
                xLabel = "Czas [s]";
                yLabel = "Czas oczekiwania w DC [ms]";
                fileName = "CloudletWaitingTime";

                xAxis = new NumberAxis();
                xAxis.setMinorTickVisible(false);
                yAxis = new NumberAxis();
                break;
            case CloudletCost.CLOUDLET_COST_STATS_ID:
                title = "Statystyka zawierająca koszt procesowania dla Cloudletów.";
                xLabel = "Czas [s]";
                yLabel = "Łączny koszt procesowania cloudleta";
                fileName = "CloudletCost";

                xAxis = new NumberAxis();
                xAxis.setMinorTickVisible(false);
                yAxis = new NumberAxis();
                break;
            default:
                Log.printLine("Chart cannot be generated. ID do not exists");
                break;
        }

        LineChart<Number, Number> lineChart = (LineChart) createChartBase(LineChart.class, xAxis, yAxis, title, xLabel, yLabel);
        for (int i = 0; i < cumulativeStats.size(); i++) {
            Class<? extends LoadBalancer> testedLb = cumulativeStats.get(i).getTestedLb();
            Map<Double, Double> data = cumulativeStats.get(i).getStatsData(statsId);
            XYChart.Series serie = createSeries(testedLb.getSimpleName(), data);
            lineChart.getData().add(serie);
        }
        saveCumulativeChart(lineChart, fileName);
    }

    private void generateChartPerLb(Stats stats) {
        switch (stats.id()) {
            case VmUsage.VM_USAGE_STATS_ID:
                Stats<UsageEntry[]> vmStats = (Stats<UsageEntry[]>) stats;
                generateUsageChart(vmStats);
                break;
            case HostUsage.VM_USAGE_STATS_ID:
                Stats<UsageEntry[]> hostStats = (Stats<UsageEntry[]>) stats;
                generateUsageChart(hostStats);
                break;
            case CloudletWaitingTime.RESPONSE_TIME_STATS_ID:
                Stats<Map<Integer, Double>> cloudletStats = (Stats<Map<Integer, Double>>) stats;
                generateCloudletChart(cloudletStats);
                break;
            case CloudletCost.CLOUDLET_COST_STATS_ID:
                Stats<Map<Integer, Double>> cloudletCost = (Stats<Map<Integer, Double>>) stats;
                generateCloudletChart(cloudletCost);
                break;
            case CloudletVmPropagation.CLOUDLET_VM_PROPAGATION_ID:
                Stats<Map<Vm, PropagationCloudletEntry>> propagation = (Stats<Map<Vm, PropagationCloudletEntry>>) stats;
                generatePropagationChart(propagation);
                break;
            default:
                Log.printLine("Chart cannot be generated. ID do not exists");
                break;
        }
    }

    private void generatePropagationChart(Stats<Map<Vm, PropagationCloudletEntry>> propagation) {
        Map<Vm, PropagationCloudletEntry> result = propagation.generateStats();
        ScatterChart<Number, Number> scatterChart = (ScatterChart) createChartBase(propagation, ScatterChart.class, new NumberAxis(), new NumberAxis());
        result.entrySet().stream()
                .sorted((vm1, vm2) -> ((Integer)vm1.getKey().getId()).compareTo(vm2.getKey().getId()))
                .forEach( entry -> {
            Vm key = entry.getKey();
            PropagationCloudletEntry value = entry.getValue();
            Map<Double, Double> serieValue = value.getUtilizationInTime().entrySet()
                    .stream()
                    .collect(Collectors.toMap(
                    e -> e.getKey(),
                    e -> e.getValue().getValue(),
                    (oldValue, newValue) -> oldValue,
                    LinkedHashMap::new
            ));
            XYChart.Series serie = createSeries("VM "+key.getId(), serieValue);
            scatterChart.getData().add(serie);
        });
        saveIndividualChart(scatterChart, propagation.header());
    }

    private void generateCloudletChart(Stats<Map<Integer, Double>> stats) {
        Map<Integer, Double> result = stats.generateStats();
        BarChart<String, Number> barChart = (BarChart) createChartBase(stats, BarChart.class, new CategoryAxis(), new NumberAxis());

        Map<String, Double> translatedMap = result.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(
                        e -> "Cloudlet " + e.getKey(),
                        e -> e.getValue(),
                        (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new
                ));

        XYChart.Series serie = createSeries("Cloudlets", translatedMap);
        barChart.getData().add(serie);
        saveIndividualChart(barChart, stats.header());
    }

    private void generateUsageChart(Stats<UsageEntry[]> stats) {
        //stage.setTitle(stats.header());
        UsageEntry[] statsResult = stats.generateStats();
        List<UsageEntry> listToSort = Arrays.asList(statsResult);
        Collections.sort(listToSort, (u1, u2) -> ((Integer)u1.getVmId()).compareTo(u2.getVmId()));
        LineChart<Number, Number> lineChart = (LineChart) createChartBase(stats, LineChart.class, new NumberAxis(), new NumberAxis());
        for (int i = 0; i < listToSort.size(); i++) {
            XYChart.Series serie = createSeries(listToSort.get(i).getComponentName(), listToSort.get(i).getUsageInTime());
            lineChart.getData().add(serie);
        }
        saveIndividualChart(lineChart, stats.header());
    }

    private void saveIndividualChart(XYChart chart, String header) {
        Scene scene = new Scene(chart, 1000, 800);
        WritableImage image = scene.snapshot(null);
        ChartManager.saveIndividualChart(testedLb, "individualExp-"+experimentNumber, image, header, "png");
    }

    private void saveCumulativeChart(XYChart chart, String header) {
        Scene scene = new Scene(chart, 1000, 800);
        WritableImage image = scene.snapshot(null);
        ChartManager.saveCumulativeChart("cumulativeExp-"+experimentNumber, image, header, "png");
    }

    public <T> XYChart.Series createSeries(String name, Map<T, Double> data) {
        XYChart.Series series = new XYChart.Series();
        series.setName(name);
        Set<Map.Entry<T, Double>> entries = data.entrySet();
        Iterator<Map.Entry<T, Double>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<T, Double> next = iterator.next();
            series.getData().add(new XYChart.Data(next.getKey(), next.getValue()));
        }
        return series;
    }

    public XYChart createChartBase(Stats stats, Class<? extends XYChart> chartKind, Axis x, Axis y) {
        x.setLabel(stats.xAxisLegend());
        y.setLabel(stats.yAxisLegend());

        XYChart xyChart = null;
        try {
            xyChart = chartKind.getConstructor(Axis.class, Axis.class).newInstance(x, y);
        } catch (Exception e) {
            e.printStackTrace();
        }

        xyChart.setAnimated(false);
        xyChart.setTitle(stats.description());
        return xyChart;
    }

    public XYChart createChartBase(Class<? extends XYChart> chartKind, Axis x, Axis y, String title, String xLabel, String yLabel) {
        x.setLabel(xLabel);
        y.setLabel(yLabel);

        XYChart xyChart = null;
        try {
            xyChart = chartKind.getConstructor(Axis.class, Axis.class).newInstance(x, y);
        } catch (Exception e) {
            e.printStackTrace();
        }
        xyChart.setAnimated(false);
        xyChart.setTitle(title);
        return xyChart;
    }
}

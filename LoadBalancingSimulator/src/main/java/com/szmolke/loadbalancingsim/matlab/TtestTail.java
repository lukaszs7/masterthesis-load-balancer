package com.szmolke.loadbalancingsim.matlab;

/**
 * @author lszmolke
 */
public enum TtestTail {
    BOTH("both"), RIGHT("right"), LEFT("left");

    String matlabVal;

    TtestTail(String matlabVal) {
        this.matlabVal = matlabVal;
    }
}
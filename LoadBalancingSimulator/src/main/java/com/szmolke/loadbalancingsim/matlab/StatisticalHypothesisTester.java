package com.szmolke.loadbalancingsim.matlab;

import com.mathworks.engine.MatlabEngine;

/**
 * @author lszmolke
 */
public class StatisticalHypothesisTester {
    private MatlabEngine matlabEngine; // matlab.engine.shareEngine('Engine_1') --> Matlab command to share session engine
    private static final double ALPHA = 0.1; // significance level

    public StatisticalHypothesisTester() {
    }

    public void connect() {
        try {
            this.matlabEngine = MatlabEngine.connectMatlab(MatlabEngine.findMatlab()[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        try {
            this.matlabEngine.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isNormalDistribution(double[] distribution) {
        Double isRejected = this.evalSWTest("Test", distribution, ALPHA);
        return isRejected == 0;
    }

    public boolean isDistrsVarianceDiffInsignificant(double[][] distributions) {
        return this.evalVartestN(distributions) > ALPHA;
    }

    public Double evalSWTest(String header, double[] distribution, double alpha) {
        try {
            System.out.println("Start evaluating SWTEST");
            this.matlabEngine.eval("[isRejected] = evaluateSWTEST('" + header + "', [" + String.join(",", this.parseDistribution(distribution)) + "], " + alpha + ");");
            return this.matlabEngine.getVariable("isRejected");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void evalTTest2(double[] distribution1, double[] distribution2, String text, TtestVarianceVartype vartype, TtestTail ttestTail) {
        try {
            System.out.println("Start evaluating TTEST2");
            this.matlabEngine.eval("evaluateTTEST2('" + text + "',[" + String.join(",", this.parseDistribution(distribution1)) + "], " +
                    "[" + String.join(",", this.parseDistribution(distribution2)) + "], " + ALPHA + ",'" + vartype.matlabVal + "','" + ttestTail.matlabVal + "');");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String evalMWWTest(double[] distribution1, double[] distribution2, String text) {
        try {
            this.matlabEngine.eval("[ret, lowerMedian] = evaluateMWWTEST('" + text + "',[" + String.join(",", this.parseDistribution(distribution1)) + "], " +
                    "[" + String.join(",", this.parseDistribution(distribution2)) + "], " + ALPHA + ");");
            String ret = this.matlabEngine.getVariable("ret");
            String lowerMedian = this.matlabEngine.getVariable("lowerMedian");

            if(ret != null) {
                return text + "|" + lowerMedian;
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String evalTTest2VarianceCheck(double[] distribution1, double[] distribution2, String text, TtestTail ttestTail) {
        TtestVarianceVartype vartype = this.isDistrsVarianceDiffInsignificant(new double[][]{distribution1, distribution2}) ? TtestVarianceVartype.EQUAL : TtestVarianceVartype.UNEQUAL;
        try {
            this.matlabEngine.eval("[ret] = evaluateTTEST2('" + text + "',[" + String.join(",", this.parseDistribution(distribution1)) + "], " +
                    "[" + String.join(",", this.parseDistribution(distribution2)) + "], " + ALPHA + ",'" + vartype.matlabVal + "','" + ttestTail.matlabVal + "');");
            String ret = this.matlabEngine.getVariable("ret");
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public Double evalVartestN(double[][] distributions) {
        try {

            String evalString = "";
            for (int i = 0; i < distributions.length; i++) {
                double[] distribution = distributions[i];
                evalString += "[" + String.join(",", this.parseDistribution(distribution)) + "]'";
                if (i <= (distributions.length - 1)) {
                    evalString += ",";
                }
            }

            this.matlabEngine.eval("evaluateVARTESTN([" + evalString + "]);");
            return this.matlabEngine.getVariable("P");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void evalTTest2(double[] distribution1, double[] distribution2, String text) {
        TtestVarianceVartype vartype = TtestVarianceVartype.EQUAL;
        TtestTail ttestTail = TtestTail.BOTH;
        this.evalTTest2(distribution1, distribution2, text, vartype, ttestTail);
    }

    private String[] parseDistribution(double[] distribution) {
        String[] strs = new String[distribution.length];
        for (int i = 0; i < distribution.length; i++) {
            strs[i] = String.valueOf(distribution[i]);
        }
        return strs;
    }

    public String evalHyphothesis(double[] distr1, double[] distr2, String text, TtestTail left) {
        if(isNormalDistribution(distr1) && isNormalDistribution(distr2)) {
            System.out.println("TTEST2");
            return evalTTest2VarianceCheck(distr1, distr2, text, left);
        } else {
            System.out.println("MWWTEST");
            return evalMWWTest(distr1, distr2, text);
        }
    }
}

package com.szmolke.loadbalancingsim.matlab;

/**
 * @author lszmolke
 */
public enum TtestVarianceVartype {
    EQUAL("equal"), UNEQUAL("unequal");

    String matlabVal;

    TtestVarianceVartype(String matlabVal) {
        this.matlabVal = matlabVal;
    }
}
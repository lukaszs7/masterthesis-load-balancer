package com.szmolke.loadbalancingsim.stats;

import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import com.szmolke.loadbalancingsim.stats.impl.CloudletCost;
import com.szmolke.loadbalancingsim.stats.impl.CloudletVmPropagation;
import com.szmolke.loadbalancingsim.stats.impl.CloudletWaitingTime;
import com.szmolke.loadbalancingsim.stats.impl.VmUsage;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.cloudbus.cloudsim.brokers.DatacenterBroker;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudsimplus.builders.tables.CloudletsTableBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Calculates simulation statistics.
 * <p>
 * Measures: 1. Cloudlets average and median execution time
 * 2. Cloudlets average and median waiting time
 * 3. Cloudlets average and median total cost
 *
 * @author lszmolke
 */
public final class StatsGenerator {

    private Map<Integer, Stats> statsCollector = new HashMap<>();

    private DatacenterBroker broker;
    private Class<? extends LoadBalancer> testedLb;
    private Median median = new Median();

    public StatsGenerator(DatacenterBroker broker, Class<? extends LoadBalancer> testedLb) {
        statsCollector.put(VmUsage.VM_USAGE_STATS_ID, new VmUsage());
        statsCollector.put(CloudletWaitingTime.RESPONSE_TIME_STATS_ID, new CloudletWaitingTime());
        statsCollector.put(CloudletCost.CLOUDLET_COST_STATS_ID, new CloudletCost());
        statsCollector.put(CloudletVmPropagation.CLOUDLET_VM_PROPAGATION_ID, new CloudletVmPropagation());
        this.broker = broker;
        this.testedLb = testedLb;
    }

    public Stats getStats(Integer statsId) {
        return this.statsCollector.get(statsId);
    }

    public Map<Integer, Stats> getStatsCollector() {
        return statsCollector;
    }

    public Map<Double, Double> getStatsData(Integer statsId) {
        Stats stats = this.getStats(statsId);
        return stats.getCumulativeStats();
    }

    public void printCollectedStatistics(Class<? extends LoadBalancer> loadBalancer) {
        // System.out.println();
        System.out.println("---------------------------------------");
        System.out.println("Statistics for Load balancer: '" + loadBalancer.getSimpleName() + "'");
        // Print built-in result tables
        this.printResultTables();
        // Print average execution time
        this.printStatistics();
//        System.out.println();
        System.out.println("---------------------------------------");
    }

    private void printStatistics() {
        double avgExecTime = cloudletsAverageExecutionTime();
        double medianExecTime = cloudletsMedianExecutionTime();
        System.out.println("Metric - execution time: AVG[" + avgExecTime+"]. MEDIAN["+medianExecTime+"].");
        double avgWaiting = cloudletsAverageWaitingTime();
        double medianWaiting = cloudletsMedianWaitingTime();
        System.out.println("Metric - waiting time: AVG[" + avgWaiting+"]. MEDIAN["+medianWaiting+"].");
        double avgCost = cloudletsAverageTotalCost();
        double medianCost = cloudletsMedianTotalCost();
        System.out.println("Metric - total cost: AVG[" + avgCost+"]. MEDIAN["+medianCost+"].");
    }

    private void printResultTables() {
        List<Cloudlet> finishedCloudlets = this.broker.getCloudletFinishedList();
//        System.out.println("STARTO");
//        finishedCloudlets.forEach(cloudlet -> {
//            if(cloudlet.getWaitingTime() != 0) {
//                System.out.println("Cloudlet WT: "+cloudlet.getWaitingTime());
//            }
//            if(cloudlet.getTotalCost() != 0) {
//                System.out.println("Cloudlet TC: "+cloudlet.getTotalCost());
//            }
//            if(cloudlet.getTotalCost() != 0) {
//                System.out.println("Cloudlet TC: "+cloudlet.get);
//            }
//        });
        System.out.println("Finished");
        new CloudletsTableBuilder(finishedCloudlets).build();

        if (finishedCloudlets.size() != this.broker.getCloudletCreatedList().size()) {
            int count = Math.abs(finishedCloudlets.size() - this.broker.getCloudletCreatedList().size());
            System.err.println(count + " cloudlets was not finished !");
        }
    }

    public double cloudletsAverageExecutionTime() {
        return this.broker.getCloudletFinishedList().stream()
                .mapToDouble(c -> c.getActualCpuTime())
                .average()
                .getAsDouble();
    }

    public double cloudletsAverageWaitingTime() {
        return this.broker.getCloudletFinishedList().stream()
                .mapToDouble(c -> c.getWaitingTime())
                .average()
                .getAsDouble();
    }

    public double cloudletsAverageTotalCost() {
        return this.broker.getCloudletFinishedList().stream()
                .mapToDouble(c -> c.getTotalCost())
                .average()
                .getAsDouble();
    }

    public double cloudletsMedianExecutionTime() {
        double[] executionTimes = this.broker.getCloudletFinishedList().stream()
                .mapToDouble(c -> c.getActualCpuTime())
                .toArray();
        return median.evaluate(executionTimes);
    }

    public double cloudletsMedianWaitingTime() {
        double[] waitingTime = this.broker.getCloudletFinishedList().stream()
                .mapToDouble(c -> c.getWaitingTime())
                .toArray();
        return median.evaluate(waitingTime);
    }

    public double cloudletsMedianTotalCost() {
        double[] cost = this.broker.getCloudletFinishedList().stream()
                .mapToDouble(c -> c.getTotalCost())
                .toArray();
        return median.evaluate(cost);
    }

    public Class<? extends LoadBalancer> getTestedLb() {
        return testedLb;
    }

    public DatacenterBroker getBroker() {
        return broker;
    }
}

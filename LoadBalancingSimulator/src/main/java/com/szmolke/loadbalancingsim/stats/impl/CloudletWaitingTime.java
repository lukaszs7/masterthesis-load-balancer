package com.szmolke.loadbalancingsim.stats.impl;

import com.szmolke.loadbalancingsim.stats.Stats;
import org.cloudsimplus.listeners.CloudletVmEventInfo;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author lszmolke
 */
public class CloudletWaitingTime implements Stats<Map<Integer, Double>> {
    public static final int RESPONSE_TIME_STATS_ID = 1002;
    private Map<Integer, Double> result = new LinkedHashMap<>();
    private Map<Double, Double> cumulativeInTime = new LinkedHashMap<>();

    public void onModifyCloudlet(CloudletVmEventInfo info) {
        result.put(info.getCloudlet().getId(), info.getCloudlet().getWaitingTime());
        this.putCumulativeValue(cumulativeInTime, Math.ceil(info.getTime()), info.getCloudlet().getWaitingTime());
    }

    @Override
    public CallbackMode callbackMode() {
        return CallbackMode.FINISH;
    }

    @Override
    public Map<Integer, Double> generateStats() {
        return result;
    }

    @Override
    public int id() {
        return RESPONSE_TIME_STATS_ID;
    }

    @Override
    public String xAxisLegend() {
        return "Cloudlet";
    }

    @Override
    public String yAxisLegend() {
        return "Czas oczekiwania w DC [ms]";
    }

    @Override
    public Map<Double, Double> getCumulativeStats() {
        return cumulativeInTime;
    }

    @Override
    public String description() {
        return "Statystyka zawierająca czas oczekiwania [ms] w DataCenter dla wszystkich Cloudletów.";
    }
}

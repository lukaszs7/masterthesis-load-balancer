package com.szmolke.loadbalancingsim.stats.dto;

import java.util.Map;

/**
 * @author lszmolke
 */
public class UsageEntry {
    private Map<Double, Double> usageInTime;
    private int vmId;
    private String componentName;

    public UsageEntry(Map<Double, Double> usageInTime, int vmId, String componentName) {
        this.usageInTime = usageInTime;
        this.vmId = vmId;
        this.componentName = componentName;
    }

    public Map<Double, Double> getUsageInTime() {
        return usageInTime;
    }

    public void setUsageInTime(Map<Double, Double> usageInTime) {
        this.usageInTime = usageInTime;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public int getVmId() {
        return vmId;
    }
}

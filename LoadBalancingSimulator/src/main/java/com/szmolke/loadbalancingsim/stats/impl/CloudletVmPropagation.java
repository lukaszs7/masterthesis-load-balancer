package com.szmolke.loadbalancingsim.stats.impl;

import com.szmolke.loadbalancingsim.stats.Stats;
import com.szmolke.loadbalancingsim.stats.dto.PropagationCloudletEntry;
import javafx.util.Pair;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.vms.Vm;
import org.cloudsimplus.listeners.CloudletVmEventInfo;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author lszmolke
 */
public class CloudletVmPropagation implements Stats<Map<Vm, PropagationCloudletEntry>> {
    public static final int CLOUDLET_VM_PROPAGATION_ID = 1006;
    private Map<Vm, PropagationCloudletEntry> propagation;

    public CloudletVmPropagation() {
        this.propagation = new LinkedHashMap<>();
    }

    public void onModifyCloudlet(CloudletVmEventInfo info) {
        Cloudlet c = info.getCloudlet();
        Vm vm = info.getVm();
        double time = info.getTime();

        PropagationCloudletEntry entry = this.propagation.get(vm);
        if (entry == null) {
            entry = new PropagationCloudletEntry();
        }
        Map<Double, Pair<Cloudlet, Double>> utilization = entry.getUtilizationInTime();
        Pair<Cloudlet, Double> utilizationForCloudlet = utilization.get(time);

        utilizationForCloudlet = new Pair(c, computeUtilization(vm, time, c));

        utilization.put(time, utilizationForCloudlet);
        propagation.put(vm, entry);
    }

    @Override
    public CallbackMode callbackMode() {
        return CallbackMode.UPDATE;
    }

    private double computeUtilization(Vm vm, double time, Cloudlet c) {
        return c.getLength();
    }

    @Override
    public Map<Vm, PropagationCloudletEntry> generateStats() {
        return propagation;
    }

    @Override
    public int id() {
        return CLOUDLET_VM_PROPAGATION_ID;
    }

    @Override
    public String description() {
        return "Statystyka zawierająca propagację zapytań dla różnych maszyn wirtualnych.";
    }

    @Override
    public String xAxisLegend() {
        return "Czas [s]";
    }

    @Override
    public String yAxisLegend() {
        return "Liczba instrukcji Cloudlet do wykonania [MI].";
    }

    @Override
    public Map<Double, Double> getCumulativeStats() {
        return null;
    }
}

package com.szmolke.loadbalancingsim.stats.dto;

import javafx.util.Pair;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author lszmolke
 */
public class PropagationCloudletEntry {
    // Values
    private Map<Double, Pair<Cloudlet, Double>> utilizationInTime;

    public PropagationCloudletEntry() {
        this.utilizationInTime = new LinkedHashMap<>();
    }

    public Map<Double, Pair<Cloudlet, Double>> getUtilizationInTime() {
        return utilizationInTime;
    }
}

package com.szmolke.loadbalancingsim.stats;

/**
 * @author lszmolke
 */
@FunctionalInterface
public interface StatsMonitorFinishCallback {
    void apply(org.cloudsimplus.listeners.CloudletVmEventInfo info);
}

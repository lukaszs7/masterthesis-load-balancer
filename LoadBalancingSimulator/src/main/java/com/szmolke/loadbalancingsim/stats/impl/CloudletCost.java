package com.szmolke.loadbalancingsim.stats.impl;

import com.szmolke.loadbalancingsim.stats.Stats;
import org.cloudsimplus.listeners.CloudletVmEventInfo;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author lszmolke
 */
public class CloudletCost implements Stats<Map<Integer, Double>> {
    public static final int CLOUDLET_COST_STATS_ID = 1005;
    private Map<Integer, Double> result = new LinkedHashMap<>();
    private Map<Double, Double> cumulativeResult = new LinkedHashMap<>();

    public void onModifyCloudlet(CloudletVmEventInfo info) {
        result.put(info.getCloudlet().getId(), info.getCloudlet().getTotalCost());
        this.putCumulativeValue(cumulativeResult, Math.ceil(info.getTime()), info.getCloudlet().getTotalCost());
    }

    @Override
    public CallbackMode callbackMode() {
        return CallbackMode.FINISH;
    }

    @Override
    public Map<Integer, Double> generateStats() {
        return result;
    }

    @Override
    public int id() {
        return CLOUDLET_COST_STATS_ID;
    }

    @Override
    public String xAxisLegend() {
        return "Cloudlet";
    }

    @Override
    public String yAxisLegend() {
        return "Łączny koszt procesowania cloudleta";
    }

    @Override
    public Map<Double, Double> getCumulativeStats() {
        return cumulativeResult;
    }

    @Override
    public String description() {
        return "Statystyka zawierająca koszt procesowania dla wszystkich Cloudletów.";
    }
}

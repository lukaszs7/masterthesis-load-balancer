package com.szmolke.loadbalancingsim.stats;

import org.cloudsimplus.listeners.CloudletVmEventInfo;

import java.util.Map;

/**
 * @author lszmolke
 */

public interface Stats<T> {

    enum CallbackMode {
        UPDATE, FINISH
    }

    void onModifyCloudlet(CloudletVmEventInfo info);
    CallbackMode callbackMode();

    int id();
    String description();
    T generateStats();
    String xAxisLegend();
    String yAxisLegend();
    Map<Double, Double> getCumulativeStats();

    default String header() {
        return getClass().getSimpleName();
    }

    default void putCumulativeValue(Map<Double, Double> cumulativeMap, Double time, Double valueToPut) {
        Double oldValue = cumulativeMap.get(time);
        if(oldValue == null) {
            cumulativeMap.put(time, valueToPut);
        } else {
            Double sumValue = (oldValue + valueToPut) / 2;
            cumulativeMap.put(time, sumValue);
        }
    }
}

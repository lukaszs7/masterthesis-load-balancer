package com.szmolke.loadbalancingsim.stats;

import com.szmolke.loadbalancingsim.algorithm.LoadBalancer;
import com.szmolke.loadbalancingsim.stats.impl.CloudletCost;
import com.szmolke.loadbalancingsim.stats.impl.CloudletWaitingTime;
import com.szmolke.loadbalancingsim.stats.impl.VmUsage;
import javafx.util.Pair;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lszmolke
 */
public class TestedLoadBalancersStats {
    private List<StatsGenerator> cumulativeLBStats = new ArrayList<>();
    private int experimentNumber;

    private static final String mainPath = "E:\\GoPro\\Snowshow 2016\\Radka";

    public TestedLoadBalancersStats(int experimentNumber) {
        this.experimentNumber = experimentNumber;
    }

    public List<Stats> retrieveCumulativeStats(Integer statsId) {
        return this.cumulativeLBStats
                .stream()
                .map(statsGenerator -> statsGenerator.getStatsCollector().get(statsId))
                .collect(Collectors.toList());
    }

    public void put(StatsGenerator statsGenerator) {
        cumulativeLBStats.add(statsGenerator);
    }

    public List<StatsGenerator> getCumulativeLBStats() {
        return cumulativeLBStats;
    }

    public void createExcel(int experimentNumber, int statsId) {
        try {
            System.out.println("START saving excel for experiment: " + experimentNumber);
            HSSFWorkbook workbook = new HSSFWorkbook();

            for (int i = 0; i < cumulativeLBStats.size(); i++) {
                StatsGenerator stats = cumulativeLBStats.get(i);
                Class<? extends LoadBalancer> testedLb = stats.getTestedLb();
                Map<Double, Double> data = stats.getStatsData(statsId);

                HSSFSheet sheet1 = workbook.createSheet(testedLb.getSimpleName());
                fillSheet(sheet1, data);

            }

            String nameStats = statsId == VmUsage.VM_USAGE_STATS_ID ? "VMUSAGE" : statsId == CloudletWaitingTime.RESPONSE_TIME_STATS_ID ? "WAITING_TIME" : "COST";

            FileOutputStream fileOut = new FileOutputStream("stats - " + nameStats + " - " + experimentNumber + ".xls");
            workbook.write(fileOut);
            fileOut.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void fillSheet(HSSFSheet sheet1, Map<Double, Double> data) {
        fillFromRow(sheet1, 0, data);
    }

    public void fillFromRow(HSSFSheet sheet, int fromRow, Map<Double, Double> data) {
        List<Pair<Double, Double>> pairs = data.entrySet().stream().map(entry -> new Pair<>(entry.getKey(), entry.getValue())).collect(Collectors.toList());

        for (int i = 0; i < pairs.size(); i++) {
            Pair<Double, Double> doubleDoublePair = pairs.get(i);
            HSSFRow row = sheet.createRow((short) i);
            HSSFCell time = row.createCell(fromRow + 0);
            HSSFCell value = row.createCell(fromRow + 1);
            time.setCellValue((double) doubleDoublePair.getKey());
            value.setCellValue((double) doubleDoublePair.getValue());
        }
    }
}

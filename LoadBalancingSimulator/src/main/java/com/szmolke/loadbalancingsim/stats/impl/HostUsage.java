package com.szmolke.loadbalancingsim.stats.impl;

import com.szmolke.loadbalancingsim.stats.Stats;
import com.szmolke.loadbalancingsim.stats.dto.UsageEntry;
import org.cloudbus.cloudsim.hosts.HostSimple;
import org.cloudbus.cloudsim.util.Log;
import org.cloudsimplus.listeners.CloudletVmEventInfo;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lszmolke
 */
public final class HostUsage implements Stats<UsageEntry[]> {
    public static final int VM_USAGE_STATS_ID = 1001;

    private List<HostSimple> hosts;

    public HostUsage(List<HostSimple> hosts) {
        this.hosts = hosts;
    }

    @Override
    public UsageEntry[] generateStats() {
        UsageEntry[] entries = new UsageEntry[hosts.size()];
        Log.printLine("\nHosts CPU utilization history for the entire simulation period");

        for (int i = 0; i < hosts.size(); i++) {
            HostSimple host = hosts.get(i);
            Map<Double, Double> data = new LinkedHashMap<>();
            double mipsByPe = host.getTotalMipsCapacity() / (double) host.getNumberOfPes();
            Log.printFormattedLine("Host %d: Number of PEs %2d, MIPS by PE %.0f", host.getId(), host.getNumberOfPes(), mipsByPe);
            Log.printLine("--------------------------------------------------");
            entries[i] = new UsageEntry(data, host.getId(), "Host "+host.getId());
        }
        return entries;
    }

    @Override
    public void onModifyCloudlet(CloudletVmEventInfo info) {

    }

    @Override
    public CallbackMode callbackMode() {
        return CallbackMode.FINISH;
    }

    @Override
    public int id() {
        return VM_USAGE_STATS_ID;
    }

    @Override
    public String description() {
        return "Statystyka zawierająca zużycie hostów w czasie - MIPS(t).";
    }

    @Override
    public String xAxisLegend() {
        return "Czas [s]";
    }

    @Override
    public String yAxisLegend() {
        return "Zużycie [MIPS]";
    }

    @Override
    public Map<Double, Double> getCumulativeStats() {
        return null;
    }
}

package com.szmolke.loadbalancingsim.stats.impl;

import com.szmolke.loadbalancingsim.stats.Stats;
import com.szmolke.loadbalancingsim.stats.dto.UsageEntry;
import javafx.util.Pair;
import org.cloudbus.cloudsim.vms.Vm;
import org.cloudbus.cloudsim.vms.VmStateHistoryEntry;
import org.cloudsimplus.listeners.CloudletVmEventInfo;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lszmolke
 */
public final class VmUsage implements Stats<UsageEntry[]> {
    public static final int VM_USAGE_STATS_ID = 1000;
    private Map<Vm, Map<Double, Double>> vmUsageMap;
    private Map<Double, Double> cumulativeUsage;

    public VmUsage() {
        this.vmUsageMap = new LinkedHashMap<>();
        this.cumulativeUsage = new LinkedHashMap<>();
    }

    /**
     * Listener on Cloudlet update
     * */
    public void onModifyCloudlet(CloudletVmEventInfo info) {

        Map<Double, Double> dataMap = this.vmUsageMap.get(info.getVm());
        if(dataMap == null) {
            dataMap = new LinkedHashMap<>();

            dataMap.put(info.getTime(), info.getVm().getTotalCpuMipsUsage(info.getTime()));

            this.vmUsageMap.put(info.getVm(), dataMap);
        } else {
            dataMap.put(info.getTime(), info.getVm().getTotalCpuMipsUsage(info.getTime()));
        }

        this.putCumulativeValue(cumulativeUsage, Math.ceil(info.getTime()), info.getVm().getTotalCpuMipsUsage(info.getTime()));
    }

    @Override
    public UsageEntry[] generateStats() {
        UsageEntry[] entries = new UsageEntry[vmUsageMap.size()];

        Set<Map.Entry<Vm, Map<Double, Double>>> data = vmUsageMap.entrySet();
        Iterator<Map.Entry<Vm, Map<Double, Double>>> iterator = data.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Map.Entry<Vm, Map<Double, Double>> entry = iterator.next();
            Vm actualVm = entry.getKey();
            Map<Double, Double> actualVmData = entry.getValue();
            entries[i++] = new UsageEntry(actualVmData, actualVm.getId(), "VM "+actualVm.getId());
        }

        return entries;
    }

    private double getVmCpuUtilizationInMips(Vm vm, double time) {
        for (VmStateHistoryEntry state : vm.getStateHistory()) {
            if (Math.abs(state.getTime() - time) <= 0.1) {
                return state.getAllocatedMips();
            }
        }
        return 0;
    }

    @Override
    public CallbackMode callbackMode() {
        return CallbackMode.UPDATE;
    }

    @Override
    public int id() {
        return VM_USAGE_STATS_ID;
    }

    @Override
    public String description() {
        return "Statystyka zawierająca Zużycie maszyn wirtualnych w czasie - MIPS(t).";
    }

    @Override
    public String xAxisLegend() {
        return "Czas [s]";
    }

    @Override
    public String yAxisLegend() {
        return "Zużycie [MIPS]";
    }

    @Override
    public Map<Double, Double> getCumulativeStats() {
        return cumulativeUsage;
    }
}

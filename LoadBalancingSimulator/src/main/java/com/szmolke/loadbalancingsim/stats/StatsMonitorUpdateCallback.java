package com.szmolke.loadbalancingsim.stats;

import org.cloudsimplus.listeners.CloudletVmEventInfo;

/**
 * @author lszmolke
 */

@FunctionalInterface
public interface StatsMonitorUpdateCallback {
    void apply(CloudletVmEventInfo info);
}

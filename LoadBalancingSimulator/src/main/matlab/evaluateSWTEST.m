function isRejected = evaluateSWTEST(name, X, alpha)
    [h,p,W] = swtest(X, alpha);
    result = 'Hipoteza Ho odrzucona';
    isRejected = 1;
    if (h == 0)
       result = 'Nie ma podstaw do odrzucenia hipotezy Ho';
       isRejected = 0;
    end
    fprintf('\nSWTEST: %s wynik: %s, p = %f, alfa = %f, W = %f\n', name, result, p, alpha, W);
end
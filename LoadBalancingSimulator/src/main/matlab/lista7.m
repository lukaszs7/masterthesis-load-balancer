    function lista7
    %zad1;
    %zad2;
    %zad3;
    %zad4;
    %zad5;
    %zad6;
    %zad7;
    zad8;
    %zad9;
end

function zad1
    data13 = [175.26,177.8,167.64000000000001,160.02,172.72,177.8,175.26,170.18,157.48,160.02,...
        193.04,149.86,157.48,157.48,190.5,157.48,182.88,160.02];
    
    data17 = [172.72,157.48,170.18,172.72,175.26,170.18,154.94,149.86,157.48,154.94,175.26,...
         167.64000000000001,157.48,157.48,154.94,177.8];
     
    evaluateSWTEST('data13', data13, 0.05);
    evaluateSWTEST('data17', data17, 0.05);
    
    alpha=0.05;
    tail='both';
    vartype='equal';   
    evaluateTTEST2(data13, data17, alpha, vartype, tail);
    
    alpha=0.1;
    tail='left';
    vartype='equal';   
    evaluateTTEST2(data13, data17, alpha, vartype, tail);
    
    alpha=0.05;
    tail='both';
    vartype='unequal';   
    evaluateTTEST2(data13, data17, alpha, vartype, tail);
    
    subplot(2,1,1);
    qqplot(data13);
    subplot(2,1,2);
    qqplot(data17);
end

function zad2
    all = load('normtemp.mat');
    all = all.normtemp;
    m = all(1:65,:);
    w = all(66:130,:);
    index = 1;
  
    evaluateTTEST2(w(:,index), m(:,index), 0.05, 'unequal', 'both');
end

function zad3
    nerwowi = [3, 3, 4, 5, 5];
    spokojni = [4, 6, 7, 9, 9];
    % h0 - nerwowi i spokojni wykonuja srednio tyle samo ruchow
    % h1 - nerwowi wykonuja srednio wiecej ruchow niz spokojni
    
    evaluateSWTEST('nerwowi', nerwowi, 0.05);
    evaluateSWTEST('spokojni', spokojni, 0.05);
    evaluateTTEST2(nerwowi, spokojni, 0.05, 'equal', 'right');
end

function zad4
    przed30 = [6, 7, 10, 9];
    po30 = [5, 6, 2, 3];
    % h0 - przed30 i po30 tak samo dowcipni
    % h1 - przed30 bardziej dowcipni niz po30
    
    evaluateSWTEST('nerwowi', przed30, 0.05);
    evaluateSWTEST('spokojni', po30, 0.05);
    evaluateTTEST2(przed30, po30, 0.05, 'equal', 'right');
end

function zad5
    all = load('normtemp.mat');
    all = all.normtemp;
    m = all(1:65,:);
    w = all(66:130,:);
    index = 1;
    
    evaluateSWTEST('mezczyzni', m(:,index), 0.05);
    evaluateSWTEST('kobiety', w(:,index), 0.05);
    evaluateTTEST2(w(:,index), m(:,index), 0.05, 'equal', 'both');
    
    %Nie mo�na przeprowadzi� t-testu dla pr�b zale�nych, poniewa� te pr�by
    %s� od siebie niezale�ne. Trzeba by zebra� ponownie pr�b� np. dla tych
    %samych os�b, ale podczas choroby.
    %Dodatkowo dla kobiet odrzucona zosta�a hipoteza o rozk�adzie normalnym
    %co przy tte�cie jest obowi�zkowe.
end

function zad6
    data13 = [175.26,177.8,167.64000000000001,160.02,172.72,177.8,175.26,...
        170.18,157.48,160.02,193.04,149.86,157.48,157.48,190.5,157.48,182.88,160.02];
    h0 = 169.051;
    
    evaluateSWTEST('data13', data13, 0.05);
    evaluateTTEST(data13, h0, 0.05);
end

function zad7
    data17 = [172.72,157.48,170.18,172.72,175.26,170.18,154.94,149.86,157.48,...
        154.94,175.26,167.64000000000001,157.48,157.48,154.94,177.8];
    h0 = 164.1475;
    
    evaluateSWTEST('data17', data17, 0.05);
    evaluateTTEST(data17, h0, 0.05);
end

function zad8
    nerwowi = [3, 3, 4, 5, 5];
    spokojni = [4, 6, 7, 9, 9];
    
    stats = mwwtest(nerwowi, spokojni);
    p = stats.p(2);
    checkTest('nerwowi i spokojni', p, 0.05);
end

function zad9
    data13 = [175.26,177.8,167.64,160.02,172.72,177.8,175.26,170.18,157.48,...
        160.02,193.04,149.86,157.48,157.48,190.5,157.48,182.88,160.02];
    
    data17 = [172.72,157.48,170.18,172.72,175.26,170.18,154.94,149.86,...
        157.48,154.94,175.26,167.64,157.48,157.48,154.94,177.8];
    
    stats = mwwtest(data13, data17);
    p = stats.p(2);
    checkTest('data13 i data17', p, 0.05);
end

function evaluateSWTEST(name, X, alpha)
    [h,p,W] = swtest(X, alpha);
    result = 'Hipoteza Ho odrzucona';
    if (h == 0)
       result = 'Nie ma podstaw do odrzucenia hipotezy Ho';
    end;
    fprintf('\nSWTEST: %s wynik: %s, p = %f, alfa = %f, W = %f\n', name, result, p, alpha, W);
end

function evaluateTTEST2(X, Y, alpha, vartype, tail)
    [h,p] = ttest2(X, Y, 'Alpha', alpha, 'Tail', tail, 'Vartype', vartype);
    result = 'Hipoteza Ho odrzucona';
    if (h == 0)
       result = 'Nie ma podstaw do odrzucenia hipotezy Ho';
    end;
    fprintf('\nwynik dla ttest2: %s - p = %f\n', result, p);
end

function evaluateTTEST(X, h0, alpha)
    [h,p] = ttest(X, h0, 'Alpha', alpha);
    result = 'Hipoteza Ho odrzucona';
    if (h == 0)
       result = 'Nie ma podstaw do odrzucenia hipotezy Ho';
    end;
    fprintf('\nwynik dla ttest: %s - p = %f\n', result, p);
end

function checkTest(name, p, alpha)
    result = 'Hipoteza Ho odrzucona';
    if (p > alpha)
       result = 'Nie ma podstaw do odrzucenia hipotezy Ho';
    end;
    fprintf('%s: , p: %f, wynik: %s\n', name, p, result);
end
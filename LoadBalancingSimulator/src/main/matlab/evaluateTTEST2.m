function ret = evaluateTTEST2(text, X, Y, alpha, vartype, tail)
    [h,p] = ttest2(X, Y, 'Alpha', alpha, 'Tail', tail, 'Vartype', vartype);
    result = 'Hipoteza Ho odrzucona';
    ret = text;
    if (h == 0)
       result = 'Nie ma podstaw do odrzucenia hipotezy Ho';
       ret = '';
    end
    fprintf('\n%s: Vartype: %s, %s - p = %f\n', text, vartype, result, p);
end
function [ret, lowerMedian] = evaluateMWWTEST(text, X, Y, alpha)
    stats = mwwtest(X, Y);
    p = stats.p(1);
    
    result = 'Hipoteza Ho odrzucona';
    ret = text;
    if (p > alpha)
       result = 'Nie ma podstaw do odrzucenia hipotezy Ho';
       ret = '';
    end
    fprintf('%s: , p: %f, wynik: %s\n', text, p, result);
    
    
    strs = strsplit(text, '<');
    lowerMedian = '';
    if median(X) < median(Y)
        lowerMedian = strs(1);
    else
        lowerMedian = strs(2);
    end
    lowerMedian = erase(lowerMedian, " ");
end

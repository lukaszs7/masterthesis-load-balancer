function P = evaluateVARTESTN(groups)
    [P,STATS] = vartestn(groups, 'Display','off');
    assignin('base', 'P', P);
end